<?php
//ACCESO PARA USUARIOS CONTROLADOR PARA DASHBOARD PRINCIPAL
require ('../Model/Conexion.php');// requiere la conexion a la base de datos de nuestro modelo
require ('Constans.php');//requiere la redireccion de los css que normalmente estarian encomtroller y use los que estan en views css
/*empezando la secion*/
if (!isset($_SESSION)) {
    session_start();
}
// CREANDO UNA VARIABLE USUARIO, los valores que se envian desde el formulario quedaran capturados con el metodo GET en esta variable
//es el mismo usuario que esta en el Loginview relacionado en el formulario
$usuario= $_GET ['usuario'];
$password= $_GET ['password'];
//variable con, se conectara la base de datos 
$con = new Conexion();
//la conexion va a buscar el usuario y clave del metodo getUser que esta en Conexion.php
$searchUser= $con->getUser($usuario,$password);
/**Logeuo del usuario  */
//asignando los datos en las variables los datos de la consulta de el metodo publico Getuser de  la tabla de la base de datos
foreach ($searchUser as $user){
    $idUsuario = $user['id_usu'];
    $tipo = $user['tipo'];
    $login = $user['login'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];

}
/**------------------cambio de color al selaccionar el menu---------------------- */
$colorElegido="#360604";
$colorDefecto="#9c4b48";
$idMenu="1";
/** opcion se dar click ensima */
$updateMenuColorElegido=$con->updateOpcionElegida($colorElegido,$idMenu);
/** color por defecto al dejar de clikear  */
$updateMenuColorDefecto=$con->updateOpcionDefecto($colorDefecto,$idMenu);

//VALIDACION DE uSUARIOS O ROLES DE USUARIO VENDEDOR, ADMINISTRADOR, ALMACEN ETC
//validacion de la existencia del Usuario y mostrando alerta de no existencia y redirige a index
if(empty($searchUser)){//si un usuario no existe activa este mensaje y retorna a index
    echo '
    <script language = javascript>
	alert("Usuario o Contraseña Incorrecta, por favor intenta de nuevo ")
	self.location = "../index.php"
	</script>
    ';
}
//--------------------------------METODO PARA VENTAS-----------------------
//si el usuario si existe en la base de datos como tipo VENTAS , INICIA UNA SECION
else if ($tipo == 'VENTAS'){
    //$_SESSION [id_usu]= $idUsuario;
      // la variable usrlViews usa la constante de Constans que usa los estilos css que estan en wievs por q en su configuracion estan diseñados para estar en otra carpeta y sera usada en la pagina de ADMINISTRADOR
      $urlViews = URL_VIEWS;
      //Declaracion de variables para mostrar en imaagen de navegador
      $userLogueado = $nombres;
      $imageUser = $foto;
      //consulta para llamar las opciones de la base de datos usando el metodo getMenuMainVentas que esta en Conexion.php declarado
    // $menuMain variable que almacena la consulta
    $menuMain = $con->getMenuMainVentas();
    require ('../Views/WellcomeVentas.php');
}
//--------------------------------METODO PARA ADMINISTRADOR-----------------------
//si el usuario si existe en la base de datos como tipo ADMINISTRADOR , INICIA UNA SECION
else if ($tipo == 'ADMINISTRADOR'){
    // la variable usrlViews usa la constante de Constans que usa los estilos css que estan en wievs por q en su configuracion estan diseñados para estar en otra carpeta y sera usada en la pagina de ADMINISTRADOR
    $urlViews = URL_VIEWS;
    //Declaracion de variables para mostrar en imaagen de navegador
    $userLogueado = $nombres;
    $imageUser = $foto;
    /** ============================================================================ */
    $fechaInicial = date('2020-10-11');
    $fechaInicialVentas =  $fechaInicial.' '. '06:00:00';
    $fechaFinal = date('2020-10-11');

    date_default_timezone_set("America/Bogota" ) ;
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinal);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaFinalVentas = $fechaVentasU.' '. '04:00:00';


    $totalVentas =$con->getDataVentasTotal($fechaInicialVentas, $fechaFinalVentas);
    //consulta para llamar las opciones de la base de datos usando el metodo getMenuMain que esta en Conexion.php declarado
    // $menuMain variable que almacena la consulta
    $menuMain = $con->getMenuMain();
    require ('../Views/Wellcome.php');
}

?>