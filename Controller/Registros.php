<?php
require('../Model/Conexion.php');
require('Constans.php');

//validacion la secion abierta//
if (!isset($_SESSION)) {
    session_start();
}
//variables que almacenan el usuario y paswword nuevos para el programa//
$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];
//pidiendo la conexion con la base de datos//
$con = new conexion();
$allUsuarios = $con->getAllUserData();
$menuMain = $con->getMenuMain();

//validacion de Usuario para no duplicar Usuarios 
if (isset($_POST['nuevo_usuario'])) {
    //capturando los valores de los imputs que se estan recibiendo desde la vista usuariosViews.php
    $usuario = $_POST['login'];
    $tipo = $_POST['tipo'];
    $nombre = $_POST['nombre'];
    $password = $_POST['password'];

    //VARIABLES PARA LAS ALERTAS DE USUARIOS-------------------------
    $mensaje = "Se Creo un nuevo usuario";
    $alerta = "alert alert-success";
    //variable para cuando se actualiza la ALERTA
    $updateMensaje = $con->updateMensajeAlert($mensaje,$alerta);


    //validcion PARA cpaturar y almacenar la imagen del Usuario nuevo-----------SI EL ARCHIVO USERFILE DEL FORMULARIO ES DIFERENTE DE VACIO GUARDE LA IMAGEN--------------
    if ($_FILES['userfile']['name'] != "") {
        $ruta = "fotoproducto/"; //AQUI QUEREMOS QUE SE GUARDE!!!!!!!!!!!!!!!!!!!!!!!!!!!! la foto
        opendir($ruta); //DECIMOS QUE SE ABRA LA RUTA
        $imagenUsuario = $ruta . $_FILES['userfile']['name']; //CUANDO RECIBE EL VALOR DEL FORMULARIO  VA A CAPURAR Y AL MACENAR EN LAS VARIABLES
        $nombre_archivo = ADDRESS . $_FILES['userfile']['name']; //ADRESS ES UNA CONSTANTE QUE ESTA EN CONTANS.PHP DONDE LO QUE RECIBIMOS DEBE ALMACENARSE EN ESA DIRECCION
        $tipo_archivo = $_FILES['userfile']['type'];
        $tamano_archivo = $_FILES['userfile']['size'];
        $nuevo_archivo = "fotoproducto/" . substr($tipo_archivo, 6, 4); //carga la foto como nueva foto
        //CONTROLANDO LOS TIPOS DE ARCHIVOS QUE SE VAN A SUBIR- SI LA IMAGEN Q SE ESTA ENVIANDO ES A LOS FORMATOS Y EL TAMAÑO ES < DE 5 MEGAS
        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");
        } else {
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $nombre_archivo)) {
                rename($nombre_archivo, $nuevo_archivo);
                // se subio correctamente
            } else {
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    } else {
        $imagenUsuario = "fotoproducto/user.png"; //si el usuario no pone foto se coloca la foto por defecto
    }
    $registerNewUser = $con->getRegisterNewUser($nombre, $tipo, $usuario, $password, $imagenUsuario);
}
// BORRAR USUARIO-----PARA EDITAR UN USER ES EN UPLOADVIEWIMAGEEDITH Y EN VISTA DE USUARIO.PHP-----------------
if (isset($_GET['idborrar'])) {

    $idUsuario = $_GET['idborrar'];
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];
    //VARIABLES PARA LAS ALERTAS DE USUARIOS-------------------------
    $mensaje = "Se Elimino un Usuario";
    $alerta = "alert alert-danger";
    //variable para cuando se actualiza la ALERTA
    $updateMensaje = $con->updateMensajeAlert($mensaje,$alerta);

    $deleteUser = $con->deleteUsuario($idUsuario);
}

//editar un usuario --------------------------------------------------------------------------------------
if (isset($_POST['update_usuario'])) {

    //capturando los valores de los imputs que se estan recibiendo desde la vista usuariosViews.php
    $idUsuarioData = $_POST['idUsuario'];
    $login = $_POST['login'];
    $tipo = $_POST['tipo'];
    $nombre = $_POST['nombre'];
    $password = $_POST['password'];
    $imagen = $_POST['imagen'];

    $usuarioLogin = $_POST['usuarioLogin'];
    $passwordLogin = $_POST['passwordLogin'];

       //VARIABLES PARA LAS ALERTAS DE USUARIOS-------------------------
       $mensaje = "Se Editaron los datos de  un Usuario ";
       $alerta = "alert alert-info";
       //variable para cuando se actualiza la ALERTA
       $updateMensaje = $con->updateMensajeAlert($mensaje,$alerta);

    //validcion PARA cpaturar y almacenar la imagen del Usuario nuevo-----------SI EL ARCHIVO USERFILE DEL FORMULARIO ES DIFERENTE DE VACIO GUARDE LA IMAGEN--------------
    if ($_FILES['userfileEdit']['name'] != "") {
        $ruta = "fotoproducto/"; //AQUI QUEREMOS QUE SE GUARDE!!!!!!!!!!!!!!!!!!!!!!!!!!!! la foto
        opendir($ruta); //DECIMOS QUE SE ABRA LA RUTA
        $imagenUsuario = $ruta . $_FILES['userfileEdit']['name']; //CUANDO RECIBE EL VALOR DEL FORMULARIO  VA A CAPURAR Y AL MACENAR EN LAS VARIABLES
        $nombre_archivo = ADDRESS . $_FILES['userfileEdit']['name']; //ADRESS ES UNA CONSTANTE QUE ESTA EN CONTANS.PHP DONDE LO QUE RECIBIMOS DEBE ALMACENARSE EN ESA DIRECCION
        $tipo_archivo = $_FILES['userfileEdit']['type'];
        $tamano_archivo = $_FILES['userfileEdit']['size'];
        $nuevo_archivo = "fotoproducto/" . substr($tipo_archivo, 6, 4); //carga la foto como nueva foto
        //CONTROLANDO LOS TIPOS DE ARCHIVOS QUE SE VAN A SUBIR- SI LA IMAGEN Q SE ESTA ENVIANDO ES A LOS FORMATOS Y EL TAMAÑO ES < DE 5 MEGAS
        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");
        } else {
            if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $nombre_archivo)) {
                rename($nombre_archivo, $nuevo_archivo);
                // se subio correctamente
            } else {
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    } 
    else { //si se a cambiado los dtaos pero no la imagen, debe conservar la vieja imagen
        $imagenUsuario = $imagen;
    }
    $updateUser = $con->updateUsuario($login, $tipo, $nombre, $password, $imagenUsuario, $idUsuarioData);
}
//una vez registrado enviamos de nuevo al formulario para observar el usuario en la interfaz 
header("Location: Usuario.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");
?>