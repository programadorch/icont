<?php
//controlador para agregar editar y borrar clientes--------------------------------------------------
require('../Model/Conexion.php');
require('Constans.php');
//inicio de session
if (!isset($_SESSION)) {
    session_start();
}
//usuario logueado -------------------------------------------------------
$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();
 //accion al oprimir el boton Registrar Del Modal Nuevo Cliente
if (isset($_POST['nuevo_cliente'])) {
    //atributos para almacenar en la BD al oprimir el boton Registrar Del Modal Nuevo Cliente
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $direccion = $_POST['direccion'];
    $telefonoFijo = $_POST['telefonoFijo'];
    $telefonoCelular = $_POST['telefonoCelular'];
    $email = $_POST['email'];
    $fechaRegistro = $_POST['fechaRegistro'];
    $ci = $_POST['ci'];
    //validando la existencia del nuevo usuario
    if($_FILES['userfile']['name']!=""){
        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfile']['name'];
        $nombre_archivo = ADDRESS . $_FILES['userfile']['name'];
        $tipo_archivo = $_FILES['userfile']['type'];
        $tamano_archivo = $_FILES['userfile']['size'];
        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);
            //validando el tipo de imagen y requerimientos
        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");
        }else{
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);               
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }
    else{//imagen por defecto
        $destino = "fotoUsuario/user.png";
    }
    //alertas en pantalla
    $mensaje = "Se registro un nuevo cliente  correctamente !!!";
    //color de la alerta
    $alerta = "alert alert-success";
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);

//llamada a la conexion BD para hacer la accion
    //$registrarNewProveedor 
    $registrarNewCliente= $con->registerNewCliente($destino,$nombre,$apellido,$direccion,$telefonoFijo,$telefonoCelular,$email,$fechaRegistro,$ci);


}
//---------------BORRAR CLIENTE------------SE ACTIVA CON EL BOTON DE LA VISTA MODAL CLIENTE
if (isset($_GET['idborrar'])) {
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];
    $idborrar = $_GET['idborrar'];
    $mensaje = "Se elimino  los datos del cliente correctamente !!!";
    $alerta = "alert alert-danger";
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //llamada a la conexion BD para hacer la accion
    $deleteClient = $con->deleteClient($idborrar);
}

// ----------------------EDITAR CLIENTE---------------------------SE ACTIVA CON EL BOTON DE LA VISTA MODAL CLIENTE
if (isset($_POST['update_cliente'])) {
    //PARAMETROS QUE SE ENVIAN ALA bd
    $idcliente = $_POST['idcliente'];
    $imagen = $_POST['imagen'];
    $usuarioLogin = $_POST['usuarioLogin'];
    $passwordLogin = $_POST['passwordLogin'];
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $direccion = $_POST['direccion'];
    $telefonoFijo = $_POST['telefonoFijo'];
    $telefonoCelular = $_POST['telefonoCelular'];
    $email = $_POST['email'];
    $fechaRegistro = date('Y-m-d');
    $ci = $_POST['ci'];


    if($_FILES['userfileEdit']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfileEdit']['name'];
        $nombre_archivo = ADDRESS . $_FILES['userfileEdit']['name'];
        $tipo_archivo = $_FILES['userfileEdit']['type'];
        $tamano_archivo = $_FILES['userfileEdit']['size'];
        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");
        }else{
            if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }
    else{
        $destino = $imagen;
    }


    $mensaje = "Se Actualizo  los datos del cliente correctamente !!!";
    $alerta = "alert alert-info";

    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //llamada a la conexion BD para hacer la accion
    //$registrarNewCliente
    $ActualizarCliente= $con->updateClient($idcliente,$destino,$nombre,$apellido,$direccion,$telefonoFijo,$telefonoCelular,$email,$fechaRegistro,$ci);
}
//---------------------------------------------------------------------------------------------------------

//usuario logueado
$searchUser = $con->getUser($usuarioLogin, $passwordLogin);
$allUsuarios = $con->getAllUserData();

foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}

//menu de la izquierda
$menuMain = $con->getMenuMain();
//localizacion de usuario
header("Location: Cliente.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");


?>
