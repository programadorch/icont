<?php
/**CUANDO ACCIONAMOS UN NUEVO PEDIDO LO QUE PASA ES QUE LOS DATOS DE LA TABLA TEMPORAL PREVENTA
 * SE ALMACENAN EN LA TABLA VENTA POR SECCIONES 1 DATOS GENERALES DE LA AFCTURA / 
 * 2 DATOS DE LOS VALORES DE LA FACTURA 3 7 DATOS DE CARACTERISTICAS DE LA FACTURA
 *  E INMEDIATAMENTE REICIAMOS O TRUNCAMOS LOS VALORES DE PREVENTA PARA INGRESAR NUEVOS VALORES DE UN NUEVO PEDIDO 
 */
require('../Model/Conexion.php');
require('Constans.php');
require_once('Codigo_control.class.php');
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_GET['RegistarVenta'])) {  //estas variables son las que estan en la factura
   
$con = new conexion();
$usuario= $_GET['usuario'];
$password = $_GET['password'];
//post es para recibir en el controlador los datos del formulario de la vista factura views
// por medio del name que esta en factura views se envia el numero de docuemnto
//opteniendo documento, total a pagar,efectivo,cambio y fecha de venta esto por que el formulairo tiene el metodo post 
$ci =$_GET['ci'];
$totalAPagar =$_GET['ingreso1']; //totalAPagar
$efectivo =$_GET['ingreso2']; //efectivo
$cambio=$_GET['resultado']; //cambio
date_default_timezone_set("America/Bogota");  // zona horaria de mi pais
$fechaVenta = date("Y-m-d H:i:s");
$estado = "NoConsolidado";
$comentario = ""; //anadiendo el comentario a registrar venta
$fechaCodigoControl = date("Ymd"); //enviando la feca actual para codigo de control

//====================transaccion  para buscar el cliente con  numero de documento del cliente========================== 
$searchClient = $con->getClienteDatos($ci);
foreach ($searchClient as $cliente) {
    //sacando los datos 
    $nombreClienteDato = $cliente['nombre']. ' ' .$cliente['apellido'];
    $idClientei = $cliente['idcliente'];
}
//envia los atributos  para INsertar  registrar a la BD por medio del modelo y el query
//$registrarDatoscliente = $con->registrarDatosPreventa($ci,$nombreClienteDato ,$totalAPagar,$efectivo,$cambio,$fechaVenta,'1');
// buscando la informacion de que usuario realiza las acciones
$searchUser = $con->getUser($usuario,$password);
foreach ($searchUser as $user) {
    //obteniendo los datos del usuario
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];  
    $login =$user['login'];   
}
$urlViews = URL_VIEWS;
$userLogueado = $nombres;
/**=================================datos iniciales de encabezado de la factura========================================== */
// muestra los datos de la BD a datos  parala factura esa parte de informacion--------------------------------------------------
$datosFactura = $con-> getDatosFactura();
// intercambio de valores  datos factura sea como factura propietario
 foreach ($datosFactura as $facturaPropieario){
     /* sacando los valores de la base de datos tabla datos, estos atributos se envian a la maquetacion para mostrarlos */
     $propietario = $facturaPropieario['propietario'];
     $razon = $facturaPropieario['razon'];
     $direccion = $facturaPropieario['direccion'];
     $nro = $facturaPropieario['nro'];
     $telefono = $facturaPropieario['telefono'];
 }


// =================consulta para mostrar el tipo de moneda ===================
$dataMoneda = $con -> getMoneda();
// mientras se cumpla la condicion se vera el contexto de la moneda y el tipo
while ($dataMonedaValues = mysqli_fetch_array($dataMoneda)){
    $contextMoneda = $dataMonedaValues['contexto'];
    $tipoMoneda = $dataMonedaValues['tipoMoneda'];
}
/**---------------------------CODIGO DE CONTROL -------------------------------------- */
//PARAMETROS QUE SE PASAN A EL CONTROLADOR GENERADOR DE CODIGO 
$codigoControl = new CodigoControl($autorizacion, $factura, $ci, $fechaCodigoControl, $totalApagar, $llave);
$getCodigoControl = $codigoControl->generar();
//---------------------************NUmero de ficha ***************************************************
date_default_timezone_set("America/Bogota");  // zona horaria de mi pais
$dateInicial =date('y-m-d');
$dateFinal =date('y-m-d');
$getNumeroFicha = $con->getNumFicha($dateInicial, $dateFinal);  //obteniendo el numero de ficha  para enviarlo a la vista
foreach ( $getNumeroFicha as $numFicha){
    $ficha = $numFicha['numficha'];
}
//==================*********** muestra los datos escojidos de la preventa********================
 $pedidoTotalPreventa = $con->getPedidoTotalForFactura();
 //  sacando cuantas filas existen para los pedidos  y en la vista con un for se recorre este paso
 $pedido = mysqli_num_rows($pedidoTotalPreventa);
 // ***************menu de la izquierda***********************
 $menuMain = $con->getMenuMain();
 //---------------- datos legales de la factura Dian numero de factura etc----------------------todo esto se envia a show factura solo las variables para usar echo-------------------------------
//-------===================----trae la informacion de la base de datos dosificacion ======================
// =almacena los datos en nuevas variables ==============
$datosDosificacion = $con->getDatosDosificacion();
foreach ($datosDosificacion as $dosificacion){
    /*Atributos de la base de datos dosificacion  */
  $autorizacion = $dosificacion['autorizacion'];
  $factura =$dosificacion['factura'];
  $llave =$dosificacion['llave'];
  $nit =$dosificacion['nit'];
  $fechaLimite=$dosificacion['fechaL'];
}
 // ==================extrae los datos del cliente de la BD ===================================
 $obtenerDatosCliente =$con->getDataCliente();
// extrae los datos de tla tabla cliente dato atravez de la funcion getdatacliente  para mostrar en la vista 
 foreach ( $obtenerDatosCliente as $datosCliente){
     $nombreCliente = $datosCliente['nombre'];
     $ci = $datosCliente['ci'];
     $fecha = $datosCliente['fecha'];
     $totalApagar = $datosCliente['totalApagar'];
     $efectivo = $datosCliente['efectivo'];
     $cambio = $datosCliente['cambio'];
 }
 //----***********************fecha limite de emision ************************************************
//--- esta fecha tiene que ser maximo un dia despues de operada para facturacion digital dian
$getDatosFecha = explode("-",$fechaLimite);  //dividiendo la fecha
$fechaLimiteAnio=$getDatosFecha[0];
$fechaLimiteMes=$getDatosFecha[1];
$fechaLimiteDia=$getDatosFecha[2];
$fechaLimiteEmision = $fechaLimiteDia.' / '.$fechaLimiteMes.' / '.$fechaLimiteAnio;  //dando orden a al nueva fecha


//--------------*********************venta total******************************-----------------
// registramos la venta  nota AL PRINCPIO Y EN EL MASTER SE ENVIA $codicocontrol pero es un objeto y toco ponerle   $getCodigoControl para que almacene el codigo de control
$registrarVentaTotal = $con->registrarVenta($nombreClienteDato, $ci, $totalAPagar, $efectivo, $cambio, $idClientei, $getCodigoControl, $fechaVenta);
    //obetenemos los datos registrados 
    $getDataClienteVenta = $con->getDatosVenta();
    foreach ($getDataClienteVenta as $dataVenta) {
        // obteniendo los datos de la BD
        $idVentas = $dataVenta['idVentas'];  //dato mas importante para la transaccion
        $nombreCliente = $dataVenta['nombre'];
        $ci = $dataVenta['ci'];
        $fecha = $dataVenta['fecha'];
        $totalApagar = $dataVenta['totalApagar'];
        $efectivo = $dataVenta['efectivo'];
        $cambio = $dataVenta['cambio'];
        $codigoControl = $dataVenta['codigoControl'];
    }
//*************insertando los datos de la factura en la bd Ventas**************************************************** */
$pedidoTotalPreventa = $con->getPedidoTotalForFactura(); // muestra los totales de el pedido de la afctura
$pedido = mysqli_num_rows($pedidoTotalPreventa); //opteniendo numero de filas 
//recorriendo los atributos de 
for ($i = 0; $i < $pedido; $i++) {
    $detallePedido = mysqli_fetch_array($pedidoTotalPreventa);
    $descripcion = $detallePedido['producto'];
    $precio = $detallePedido['precio'];
    $cantidad = $detallePedido['cantidad'];
    $total = $detallePedido['precio'] * $detallePedido['cantidad'];
    $tipo = $detallePedido['tipo'];
    // recorriendo con el for e einsertando los datps en la base de datos  datos venta
    $registrarPreventaTotalFinal = $con->registrarDatosVenta($cantidad, $descripcion, $precio, $total, $tipo, $fechaVenta, $codigoControl, $idVentas, $estado);
}
/**Divide los datos de la factura en 3 y los almacena en la BD, las variables mencionadas son las que estan declaradas en el primer if */
//ALMACENA LOS VALORES DE L FACTURA
$registrarDatosVentaTotal = $con->registrarDatosVentaTotal($nombreClienteDato, $cantidad, $precio, $total, $codigoControl, $fechaVenta, $estado,$comentario);
//ALAMACENA LOS DATOS DEL CLIENTE
$registrarClienteDatosFinal = $con->registrarDatosClienteVenta($fechaVenta, $ci, $nombreClienteDato, $codigoControl, $idVentas, $estado);
//ALMACENA LOS DATOS DE LA FACTURA  SON COMO LAS CARACTERISTICAS DE LA FACTURA
$registrarFacturaDatosFinal = $con->registrarDatosFacturaVenta($nit, $factura, $autorizacion, $codigoControl, $idVentas, $estado);


 /// Clean for new client  / BORRAS LOS DATOS DEL ANTOGUO CLIENTE 
/** BORRA LOS DATOS DE CLIENTE ANTIGUO */
 $cleanDataCliente = $con->cleanClientData();
 /**BORRA LOS DATOS DE LA FACTURA */
 $cleanDataPreventa = $con->cleanRegistroPreventa();
/** REDIRECCIONAR A CONTROLADOR VENTAS.PHP ENVIANDO USUARIO Y PASWWORD PARA VOLVER A LA SECCION DE VENTAS */
header("Location: Ventas.php?usuario=$login&password=$password");

}
