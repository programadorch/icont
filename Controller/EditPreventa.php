<?php
require('../Model/Conexion.php');
require('Constans.php'); 

if (!isset($_SESSION)) {
    session_start();
}
//PARAMETROS GLOBALES 
$idProducto = $_POST['idProducto'];
$tipo = $_POST['tipo'];
$idUsuario = $_POST['idUser'];
// CREANDO UNNUEVO OBJETO DE CONEXION
$con = new conexion();
// OBTIENE EL ID DEL USUARIO QUE REALIZA LA ACCION
$onlyUserSession = $con->getOnlyUserData($idUsuario);
// extrae  el usuario, contraseña y tipo de usuario  
foreach ($onlyUserSession as $user) {
    $usuario = $user['login'];
    $password = $user['password'];
    $tipoUsuserio = $user['tipo']; // estos valores son los que se vuelven dinamicos y se envian al inspeccionar el formulario para tener un informe de quien edita
}
$urlViews = URL_VIEWS;
//=================================================================================================
// OBTIENE EL ID DE PRODUCTO Y EL TIPO  "MESA || LLEVAR"
$editPreVentaData = $con->getDataProductoChoose($idProducto, $tipo); //LOSMISMOS PARAMETROS DE LA FUNCION
//REASIGNA O SACA  LOS PARAMETROS REALES DE LA BASE DE DATOS EN UNA NUEVA VARIABLE LLAMADA PREVENTA
//sacando los pdocutos que se van a editar
foreach ($editPreVentaData as $preVenta) {
    $idPreventa = $preVenta['idPreventa'];
    $imagen = $preVenta['imagen'];
    $producto = $preVenta['producto']; //atributos dinamicos de la base de datos que sirven para mostrar un informe de los prodcutos 
    $precio = $preVenta['precio'];
    $idProducto = $preVenta['idProducto'];
    $pventa = $preVenta['pventa'];
    $userId = $preVenta['idUser'];
    $tipoPedido = $preVenta['tipo'];
}
//sacando la CANTIDAD TOTAL DEL PRODUCTO QUE VAMOS A EDITAR
$getCantidad = $con->getCantidadProductoChoose($idProducto, $tipo);
//sacando ese total en una variable
foreach ($getCantidad as $getCantidadTotal) {
    $cantidadActual = $getCantidadTotal['cantidadTotal']; // la cantidad que vamos a editar del producto
}

// llamando al vista formulario al que se va a enviar la edicion
require('../Views/EditPreVentaForm.php');
?>
