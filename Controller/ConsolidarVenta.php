<?php
require('../Model/Conexion.php');
require('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();
/** ---------CAMBIAR ESTADO NO CONSOLIDADO A CONSOLIDADO EN LAS TABLAS-------------- */
// cuando de click en el  boton de consolidar en la vistasonsolidarviews.php  el boton verde
if (isset($_GET['idConsolidar'])) {
    $idVenta = $_GET['idConsolidar'];
    $codigoControl = $_GET['codigoControl']; /** muy importamte esta variable que obtiene el codigo de control que envia la vista aqui se recibe */
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];
    // actualiza el estado de no consolidado a consolidado en las siguientes tablas 
    $updateDatosclienteventa = $con->updateDatosclienteventa($codigoControl);
    $updateDatosfacturaventa = $con->updateDatosfacturaventa($codigoControl);
    $updateDatosventa = $con->updateDatosventa($codigoControl);
    $updateDatosventatotal = $con->updateDatosventatotal($codigoControl);
    //  mensaje de alerta 
    $mensaje = "Se Consolido la venta  correctamente !!!";
    $alerta = "alert alert-success";
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);

}
/** ----------INSERTAR COMENTARIO ---en consolidacion----------------------------------------- */
/** insertar comentario es el name del boton en la vista es el boton amarillo */
if (isset($_POST['insertarComentario'])) {
    $idVentas = $_POST['idVentas']; /** se hace atravez del idventas */
    $comentario = $_POST['comentario']; /**captura el comentario escrito en la vista */
    /** fuoncion que inserta el comentario en la tabla Datos venta total */ 
    $updateComentario = $con ->insertarComentarioFicha($idVentas, $comentario); /**datos obtenidos para comparar */
    /**Mensaje de confirmacion */
    $mensaje = "Se Inserto un comentario correctamente !!!";
    $alerta = "alert alert-info";
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
}

$searchUser = $con->getUser($usuarioLogin, $passwordLogin);
$allUsuarios = $con->getAllUserData();

foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}

$menuMain = $con->getMenuMain();
header("Location: Consolidar.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");

?>
