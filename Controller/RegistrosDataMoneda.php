<?php
require('../Model/Conexion.php');
require('Constans.php');
//valida si el usuario esta logueado
if (!isset($_SESSION)) {
    session_start();
}
//variables de usuario logueado
$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];
//conexion con BD
$con = new conexion();

$allUsuarios = $con->getAllUserData();
$menuMain = $con->getMenuMain();
// si la moneda  es = al tipo de moneda seleccionada entonces muestre los correspondiente update_data_moneda es el nombre del boton actualizar moneda
if (isset($_POST['update_data_moneda'])) {
    $usuarioLogin = $_POST['usuarioLogin'];
    $passwordLogin = $_POST['passwordLogin'];
    $idMoneda = $_POST['idMoneda'];
    $moneda = $_POST['moneda'];

    if ($moneda == "Argentina") {
        $pais = "Argentina";
        $tipoMoneda = "$";
        $contexto = "Peso argentino";
    }

    if ($moneda == "EUA") {
        $pais = "Estados Unidos";
        $tipoMoneda = "$ USD";
        $contexto = "Dólar estadounidense";
    }
    if ($moneda == "Bolivia") {
        $pais = "Bolivia";
        $tipoMoneda = "Bs.";
        $contexto = "Bolivianos";
    }
    if ($moneda == "Ecuador") {
        $pais = "Ecuador";
        $tipoMoneda = "$";
        $contexto = "Dólar estadounidense";
    }
    if ($moneda == "Colombia") {
        $pais = "Colombia";
        $tipoMoneda = "$";
        $contexto = "Peso colombiano";
    }
    if ($moneda == "Peru") {
        $pais = "Peru";
        $tipoMoneda = "S/";
        $contexto = "Sol";
    }
    if ($moneda == "Brasil") {
        $pais = "Brasil";
        $tipoMoneda = "R$";
        $contexto = "Real brasileño";
    }
    if ($moneda == "Chile") {
        $pais = "Chile";
        $tipoMoneda = "$";
        $contexto = "Peso chileno	";
    }
    if ($moneda == "Venezuela") {
        $pais = "Venezuela";
        $tipoMoneda = "Bs F";
        $contexto = "Bolívar fuerte";
    }
    if ($moneda == "Mexico") {
        $pais = "Mexico";
        $tipoMoneda = "$";
        $contexto = "Peso mexicano";
    }
    if ($moneda == "Espania") {
        $pais = "Espania";
        $tipoMoneda = "€";
        $contexto = "Euro";
    }
    if ($moneda == "Paraguay") {
        $pais = "Paraguay";
        $tipoMoneda = "₲";
        $contexto = "Guaraní paraguayo";
    }
    if ($moneda == "Uruguay") {
        $pais = "Uruguay";
        $tipoMoneda = "$";
        $contexto = "Peso uruguayo";
    }

    //alerta 
    $mensaje = "Se Actualizo  los datos de la Moneda correctamente a:  $contexto"; 
    $alerta = "alert alert-info";
    //usa el modelo para mostrar el mensaje de actualizacion
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //usa la conexion de la BD moneda donde se hace un Update en comexion.php  updateDataMoneda
    $updateDatosMoneda = $con->updateDataMoneda($idMoneda, $pais, $tipoMoneda, $contexto);

}
//identifica en la parte inferior el tipo de moneda
header("Location: Moneda.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");

?>
