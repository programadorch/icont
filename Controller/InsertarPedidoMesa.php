<?php
require('../Model/Conexion.php');
require('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}

//datos que estamos enviando 
$idProducto = $_GET['idproducto'];
$idUsuario = $_GET['iduser'];

$con = new conexion();
// -----------------------------------------------------------------------------------------------
//  recibe el id del usuario que hace la trasaccion
$onlyUserSession = $con->getOnlyUserData($idUsuario);
// verifica que los datos del usuario logueado sean correctos
foreach ($onlyUserSession as $user) {
    
    $usuario = $user['login'];
    $password = $user['password'];
}
// producto elejido para la preventa
$productElegido = $con->getProductoElegido($idProducto);
// busca y captura los atributos de los productos de la base de datos producto y los envia a preventa 
foreach ($productElegido as $product) {
    $idProducto = $product['idproducto'];
    $imagen = $product['imagen'];
    $codigo = $product['codigo'];
    $nombreProducto = $product['nombreProducto'];
    $cantidad = $product['cantidad'];
    $fechaRegistro = date('Y-m-d H:i:s');
    $precioVenta = $product['precioVenta'];
    $tipo = $product['tipo'];
}
//variable que envia ala base de datos preventa el tipo de pedido
$tipoPedido = 'Mesa';
//ruta para enlazar imagenes
$urlViews = URL_VIEWS;
//registranso productos para la PreVenta
$regiterPreventa = $con->insertarPreventaProducto($imagen, $nombreProducto, $precioVenta, $idProducto, $precioVenta, $idUsuario, $tipoPedido);
//refresca el pedido o actualiza la vista de pedido
require('../Views/RefreshPedido.php');
?>
