
<?php
/** controlador de la vista general de reportes graficos  */
require('../Model/Conexion.php');
require('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuario'];
$password = $_GET['password'];

$con = new Conexion();


$searchUser = $con->getUser($usuario, $password);
$allUsuarios = $con->getAllUserData();
/**-------------------------------------------------------------------- */
foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}

$tipoDeAlerta = $con->getMensajeAlerta();
foreach ($tipoDeAlerta as $tipoAlerta) {
    $alerta = $tipoAlerta['tipoAlerta'];
    $mensaje = $tipoAlerta['mensaje'];
}

if (!isset($_GET['estado'])) {
    $mensaje = "";
    $alerta = "";

    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
}


$urlViews = URL_VIEWS;
$userLogueado = $nombres;
$imageUser = $foto;
/**-----VENTAS MENSUALES ------------MUESTRA LOS TOTALES POR MESES EN VENTAS ----------SOLO CUANDO ES CONSOLIDADO------------------------------------------- */
$ventasMensuales = $con->getVentasMensuales();
/**------VENTAS MENSUALES-----MUESTRA UN LISTADO DE LOS MESES  DE LA  TABLA VENTA TOTAL ----------------------------------------------------------- */
$sumVentasMensuales = $con->getSumTotalVentasMensuales();
/**----MUESTRA LA SUMA DE LOS TOTALES DE VENTAS DE TODO EL MES-------------------------------------------------- */
$totalVentasMensual = $con->getTotalVentasMensual();
$menuMain = $con->getMenuMain();


require("../Views/ReporteGraficos.php");
?>