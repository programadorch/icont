<?php
//controlador para agregar editar y borrar clientes--------------------------------------------------
require('../Model/Conexion.php');
require('Constans.php');
//inicio de session
if (!isset($_SESSION)) {
    session_start();
}
//usuario logueado -------------------------------------------------------
$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];
$con = new conexion();

 //accion al oprimir el boton Registrar Nuevo se activa con el name nuevo_producto producto en ProductoViews.php
if (isset($_POST['nuevo_Producto'])) {
    //estos atributos son losque capturan los imptus y lo que esta en corchete son los NAME de imputs de el modal de agregar producto en la vista
    //atributos para almacenar en la BD al oprimir el boton Registrar Del Modal Nuevo Cliente     
     $tipoproducto = $_POST['tipoproducto'];// esta variable esta en el while de la vista de prodcutos
     $codigo= $_POST['codigo'];
     $nombreProducto= $_POST['descripcion'];
     $cantidad = $_POST['cantidad'];
     $precioVenta = $_POST['pventa'];
     $precioCompra = $_POST['pcompra'];
     $fechaRegistro = $_POST['fechaRegistro'];
     $proveedor =null;
    //-----------------validando para cargue de imagenes--------------------------------------------
    if($_FILES['userfile']['name']!=""){
        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfile']['name'];
        $nombre_archivo = ADDRESS . $_FILES['userfile']['name'];
        $tipo_archivo = $_FILES['userfile']['type'];
        $tamano_archivo = $_FILES['userfile']['size'];
        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);
            //validando el tipo de imagen y requerimientos
        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");
        }else{
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);               
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }
    else{//imagen por defecto
        $destino = "fotoUsuario/user.png";
    }//_---------------------------------------------------------------------------
    //alertas en pantalla
    $mensaje = "Se registro un nuevo Producto correctamente !!!";
    //color de la alerta
    $alerta = "alert alert-success";
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);

//llamada a la conexion BD para hacer la accion de registrar un nuevo producto en la base de datos despues de capturar la informacion  
$registerNewProducto = $con->registerNewProducto($destino,$codigo,$nombreProducto,$cantidad,$fechaRegistro,$precioVenta,$tipoproducto,$proveedor,$precioCompra);


}
//---------------BORRAR PRODUCTO------------SE ACTIVA CON EL BOTON DE LA VISTA MODAL PRODYCTOVIEWS.PHP
if (isset($_GET['idborrar'])) {
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];
    $idborrar = $_GET['idborrar'];
    $mensaje = "Se elimino  los datos del Producto correctamente !!!";
    $alerta = "alert alert-danger";
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //llamada a la conexion BD para hacer la accion de eliminar el producto del id especifico
    $deleteProducto = $con->deleteProduct($idborrar);
}

// ----------------------EDITAR PRODUCTO---------------------------SE ACTIVA CON EL BOTON DE LA VISTA MODAL CLIENTE
if (isset($_POST['update_producto'])) {
    //PARAMETROS QUE SE ENVIAN ALA bd
    //estos atributos son losque capturan los imptus y lo que esta en corchete son los NAME de imputs de el modal de agregar producto en la vista
    //atributos para almacenar en la BD al oprimir el boton Registrar Del Modal Nuevo Cliente
    $idproducto = $_POST['idproducto'];  // esta variable almacena el NAME de el Botton Verde 
    $imagen = $_POST['imagen'];
    $tipoproducto = $_POST['tipoproducto']; // esta variable esta en el while de la vista de prodcutos
    $codigo= $_POST['codigo'];
    $nombreProducto= $_POST['descripcion'];
    $cantidad = $_POST['cantidad'];
    $precioVenta = $_POST['pventa'];
    $precioCompra = $_POST['pcompra'];
    $fechaRegistro = date("Y-m-d"); //fecha automatica
    $proveedor =null;

    if($_FILES['userfileEdit']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfileEdit']['name'];
        $nombre_archivo = ADDRESS . $_FILES['userfileEdit']['name'];
        $tipo_archivo = $_FILES['userfileEdit']['type'];
        $tamano_archivo = $_FILES['userfileEdit']['size'];
        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);//aqui guarda la nueva foto


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");
        }else{
            if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }
    else{
        $destino = $imagen;
    }


    $mensaje = "Se Actualizo  los datos del Producto correctamente !!!";
    $alerta = "alert alert-info";

    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //llamada a la conexion BD para hacer la accion ACTUALIZAR     
    $updateProductoData = $con->updateProduct($imagen,$codigo,$nombreProducto,$cantidad,$fechaRegistro,$precioVenta,$tipoproducto,$proveedor,$precioCompra,$idproducto);
}
//---------------------------------------------------------------------------------------------------------

//usuario logueado
$searchUser = $con->getUser($usuarioLogin, $passwordLogin);
$allUsuarios = $con->getAllUserData();

foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}

//menu de la izquierda
$menuMain = $con->getMenuMain();
//localizacion de usuario
header("Location: Producto.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");


?>
