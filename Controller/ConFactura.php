<?php
require('../Model/Conexion.php');
require('Constans.php');
require_once('Codigo_control.class.php');
if (!isset($_SESSION)) {
    session_start();
}
$con = new conexion();
$usuario= $_GET['usuario'];
$password = $_GET['password'];

//post es para recibir en el controlador los datos del formulario de la vista factura views
// por medio del name que esta en factura views se envia el numero de docuemnto
//opteniendo documento, total a pagar,efectivo,cambio y fecha de venta esto por que el formulairo tiene el metodo post 
$ci =$_GET['ci'];
$totalAPagar =$_GET['ingreso1']; //totalAPagar
$efectivo =$_GET['ingreso2']; //efectivo
$cambio=$_GET['resultado']; //cambio
date_default_timezone_set("America/Bogota");  // zona horaria de mi pais
$fechaVenta = date("Y-m-d H:i:s");
$fechaCodigoControl = date("Ymd");
//====================transaccion  para buscar el  numero de documento del cliente========================== 
$searchClient = $con->getClienteDatos($ci);
foreach ($searchClient as $cliente) {
    $nombreClienteDato = $cliente['apellido']. ' ' .$cliente['apellido'];
}
//envia los atributos  para INsertar  registrar a la BD por medio del modelo y el query
$registrarDatoscliente = $con->registrarDatosPreventa($ci,$nombreClienteDato ,$totalAPagar,$efectivo,$cambio,$fechaVenta,'1');
$searchUser = $con->getUser($usuario,$password);
foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}
$urlViews = URL_VIEWS;
$userLogueado = $nombres;
$imageUser = $foto;
/**=================================datos iniciales de la factura========================================== */
// muestra los datos de la BD a datos  parala factura esa parte de informacion--------------------------------------------------
$datosFactura = $con-> getDatosFactura();
// intercambio de valores  datos factura sea como factura propietario
 foreach ($datosFactura as $facturaPropieario){
     /* sacando los valores de la base de datos tabla datos, estos atributos se envian a la maquetacion para mostrarlos */
     $propietario = $facturaPropieario['propietario'];
     $razon = $facturaPropieario['razon'];
     $direccion = $facturaPropieario['direccion'];
     $nro = $facturaPropieario['nro'];
     $telefono = $facturaPropieario['telefono'];
 }
 //---------------- datos legales de la factura Dian numero de factura etc----------------------todo esto se envia a show factura solo las variables para usar echo-------------------------------
//-------===================----trae la informacion de la base de datos dosificacion ======================
// =almacena los datos en nuevas variables ==============
 $datosDosificacion = $con->getDatosDosificacion();
 foreach ($datosDosificacion as $dosificacion){
     /*Atributos de la base de datos dosificacion  */
   $autorizacion = $dosificacion['autorizacion'];
   $factura =$dosificacion['factura'];
   $llave =$dosificacion['llave'];
   $nit =$dosificacion['nit'];
   $fechaLimite=$dosificacion['fechaL'];
 }
 // ==================extrae los datos del cliente de la BD ===================================
 $obtenerDatosCliente =$con->getDataCliente();
// extrae los datos de tla tabla cliente dato atravez de la funcion getdatacliente  para mostrar en la vista 
 foreach ( $obtenerDatosCliente as $datosCliente){
     $nombreCliente = $datosCliente['nombre'];
     $ci = $datosCliente['ci'];
     $fecha = $datosCliente['fecha'];
     $totalApagar = $datosCliente['totalApagar'];
     $efectivo = $datosCliente['efectivo'];
     $cambio = $datosCliente['cambio'];
 }
//================== muestra los datos escojidos de la preventa================
 $pedidoTotalPreventa = $con->getPedidoTotalForFactura();
 //  sacando cuantas filas existen para los pedidos  y en la vista con un for se recorre este paso
 $pedido = mysqli_num_rows($pedidoTotalPreventa);
// =================consulta para mostrar el tipo de moneda en letras===================
$dataMoneda = $con -> getMoneda();
// mientras se cumpla la condicion se vera el contexto de la moneda y el tipo
while ($dataMonedaValues = mysqli_fetch_array($dataMoneda)){
    $contextMoneda = $dataMonedaValues['contexto'];
    $tipoMoneda = $dataMonedaValues['tipoMoneda'];
}
 /********************************************CODIGO DE CONTROL ********************************* */
 
 //PARAMETROS QUE SE PASAN A EL CONTROLADOR GENERADOR DE CODIGO 
 $codigoControl = new CodigoControl($autorizacion, $factura, $ci, $fechaCodigoControl, $totalApagar, $llave);
  $getCodigoControl = $codigoControl->generar();
//----***********************fecha limite de emision ************************************************
//--- esta fecha tiene que ser maximo un dia despues de operada para facturacion digital dian
$getDatosFecha = explode("-",$fechaLimite);  //dividiendo la fecha
$fechaLimiteAnio=$getDatosFecha[0];
$fechaLimiteMes=$getDatosFecha[1];
$fechaLimiteDia=$getDatosFecha[2];
$fechaLimiteEmision = $fechaLimiteDia.' / '.$fechaLimiteMes.' / '.$fechaLimiteAnio;  //dando orden a al nueva fecha
//---------------------************NUmero de ficha ***************************************************
date_default_timezone_set("America/Bogota");  // zona horaria de mi pais
// efchas de hoy que enviamos al modelo para obtener numero de ficha
$dateInicial =date('y-m-d');
$dateFinal =date('y-m-d');
$getNumeroFicha = $con->getNumFicha($dateInicial, $dateFinal);  //obteniendo el numero de ficha  para enviarlo a la vista
foreach ( $getNumeroFicha as $numFicha){
    $ficha = $numFicha['numficha']; // enviando el valor a la vista de la ficha
}


$menuMain = $con->getMenuMain();
require('../Views/ConFacturaViews.php');
?>





