<?php
require('../Model/Conexion.php');
require('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuarioLogin'];
$password = $_GET['passwordLogin'];
$anio = $_GET['anio'];
$mes = $_GET['mes'];

$con = new Conexion();


$searchUser = $con->getUser($usuario, $password);
$allUsuarios = $con->getAllUserData();

foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}

$tipoDeAlerta = $con->getMensajeAlerta();
foreach ($tipoDeAlerta as $tipoAlerta) {
    $alerta = $tipoAlerta['tipoAlerta'];
    $mensaje = $tipoAlerta['mensaje'];
}

if (!isset($_GET['estado'])) {
    $mensaje = "";
    $alerta = "";

    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
}


$urlViews = URL_VIEWS;
$userLogueado = $nombres;
$imageUser = $foto;
$menuMain = $con->getMenuMain();

/**  --Muestra el mes seleccionado-----toma el nombre del mes y lo muestra como parametro mes desde tabla datosventatotal y ordena por meses en orden ascendente------------ */
$ventasMensuales = $con->getVentasMensuales();
/**------VENTAS MENSUALES-----MUESTRA UN LISTADO DE LOS MESES  DE LA  TABLA VENTA TOTAL ----------------------------------------------------------- */
$sumVentasMensuales = $con->getSumTotalVentasMensuales();
/**-------------muestra el mes y el año para seleccionarlo en la vista ------------- */
$totalVentasMensual = $con->getTotalVentasByMes($mes,$anio);

require('../Views/EstadisticaViewsMes.php');

?>