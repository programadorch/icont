<?php
// estrcutura del CRUD o BACKEND
require('../Model/Conexion.php');
require('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}

$con = new conexion();
//cuabdo en la vista haga click en el boton registrar se activara esta validacion prooverdorviews
if (isset($_POST['new_proveedor'])) {
    //pide los datios del proveedor que son los atributos de la tabla
    $usuarioLogin = $_POST['usuarioLogin'];
    $passwordLogin = $_POST['passwordLogin'];
    $proveedor = $_POST['proveedor'];
    $responsable = $_POST['responsable'];
    $fechaRegistro = $_POST['fechaRegistro'];
    $direccion = $_POST['direccion'];
    $telefono = $_POST['direccion'];

    $mensaje = "Se Actualizo  los datos de la Proveedor correctamente !!!";
    $alerta = "alert alert-success";
    //funcion del modelo para ver alertas
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //funcion del modelo para agregar nuevo proveedor
    $registrarNewProveedor = $con->registerNewProveedor($proveedor, $responsable, $direccion, $telefono, $fechaRegistro);


}

//cuando en la vistaproveedor le damos click en boton borrar se activa este if por idborrar
if (isset($_GET['idborrar'])) {
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];
    $idProveedor = $_GET['idborrar'];

    $mensaje = "Se elimino  los datos de la Proveedor correctamente !!!";
    $alerta = "alert alert-danger";
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //funcion del modelo para  eliminar un proveedor
   $deleteProveedor = $con->deleteProveedor($idProveedor);


}
// cuando se da click en boton actualizar se activa este if en proveedorviews
if (isset($_POST['update_proveedor'])) {
    $idProveedor = $_POST['idproveedor'];
    $usuarioLogin = $_POST['usuarioLogin'];
    $passwordLogin = $_POST['passwordLogin'];
    $proveedor = $_POST['proveedor'];
    $responsable = $_POST['responsable'];
    $fechaRegistro = $_POST['fechaRegistro'];
    $direccion = $_POST['direccion'];
    $telefono = $_POST['direccion'];

    $mensaje = "Se Actualizo  los datos de la Proveedor correctamente !!!";
    $alerta = "alert alert-info";
       
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
             //funcion del modelo para  ACTUALIZAR un proveedor
    $registrarNewProveedor = $con->updateProveedor($idProveedor, $proveedor, $responsable, $direccion, $telefono, $fechaRegistro);


}
// USUARIO LOGUEADO
$searchUser = $con->getUser($usuarioLogin, $passwordLogin);
$allUsuarios = $con->getAllUserData();

foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}


$menuMain = $con->getMenuMain();
//LOCALIZACION PARA REDIRECCIONAR Y MOSTRAR ALERTA
header("Location: Proveedor.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");


?>
