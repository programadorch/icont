<?php
/** controlasdor de la vista general de los reportes pdf */
require('../Model/Conexion.php');
require('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuarioLogin'];
$password = $_GET['passwordLogin'];
$con = new Conexion();

/**--------------Controlador paraeporte del dia ---------------------------- */
/** todo lo qu este  [] dentro de los if  son los name de los botones en las vistas */
/** si existe el lcikeo en la vista del name reporte_dia  entonces hacer las siguientes acciones*/
if (isset($_GET['reporte_dia'])) {
    /** caputa con el get la fceha de venta */
    $fechaVentas = $_GET['fechaVentas'];
    /**la feca de hoy a la primera hora */
    $fechaVentasInicial = $fechaVentas . ' ' . '00:01:00';

    date_default_timezone_set("America/Bogota");
    $tiempo = getdate(time());
    $fecha = date_create($fechaVentas);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaVentasFinal = $fechaVentasU . ' ' . '23:59:00';
    /**  obtiene o muestra la fecha actual consolidado */
    $ventasByDia = $con->getVentasDia($fechaVentasInicial, $fechaVentasFinal);
   /**muetra  la sumatoria las ventas consolidadas del dia actual */
    $ventasTotalByDia = $con->getVentasTotalesDia($fechaVentasInicial, $fechaVentasFinal);

        /** redirigiendo a la vista de el reporte pdf diario */
    require('../Views/ReporteVentasPorDia.php');
}

/**--------------Controlador paraeporte por rango de fechas ----------------------------*/

if (isset($_GET['rango_fecha'])) {

    $fechaVentas = $_GET['fechaInicialVentas'];
    $fechaVentasInicial = $fechaVentas . ' ' . '00:01:00';

    $fechaFinalVentas = $_GET['fechaFinalVentas'];

    date_default_timezone_set("America/Bogota");
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinalVentas);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');
    $fechaVentasFinal = $fechaVentasU . ' ' . '23:59:00';

    $ventasByDia = $con->getVentasDia($fechaVentasInicial, $fechaVentasFinal);
    $ventasTotalByDia = $con->getVentasTotalesDia($fechaVentasInicial, $fechaVentasFinal);
    require('../Views/ReporteVentasPorDia.php');
}
/**--------------Controlador para reporte por productos ----------------------------*/

if (isset($_GET['reporte_producto'])) {
    $fechaVentas = $_GET['fechaInicialVentas'];
    $fechaVentasInicial = $fechaVentas . ' ' . '00:01:00';
    /** ['fechaFinalVentas'] es el name de el calendario en la vista y el valor que se escoje alli*/
    $fechaFinalVentas = $_GET['fechaFinalVentas'];
    date_default_timezone_set("America/Bogota");
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinalVentas);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaVentasFinal = $fechaVentasU . ' ' . '23:59:00';
    /**venta de productos en el dia */
    $ventasProductoByDia = $con->getVentasProductoByDia($fechaVentasInicial, $fechaVentasFinal);
   /** suma de la venta de productos  */
    $ventasTotalProductoByDia = $con->getVentasProductoTotalesDia($fechaVentasInicial, $fechaVentasFinal);

    require('../Views/ReporteProductosVentasPorDia.php');
}
/**--------------Controlador para reporte por mes ----------------------------*/
if (isset($_GET['reporte_mes'])) {
    /** ['anio']; ['mes']; son los name de los imputs en la vista */
     $anio = $_GET['anio'];
     $mes = $_GET['mes'];
     /**-----muestra el mes ------ */
    $ventasMensuales = $con->getVentasMensuales();
    /**-------muestra  el dia especifico del y el total de vendido en el dia esto es la tabla de resporte------------ */
    $sumVentasByMes = $con->getSumaTotalVentasByMes($mes,$anio);
   /**------muestra la suma mde las ventas totales del mes esto es el texto negro al final de la tabla-------- */
    $totalVentasMensual = $con->getTotalVentasByMes($mes,$anio);

    require('../Views/ReporteVentasPorMes.php');
}
/**--------------Controlador para reporte de utilidad o ganancia  ----------------------------*/
if (isset($_GET['utilidad'])) {

    $fechaInicial = $_GET['fechaInicialVentas'];
    $fechaInicialVentas =  $fechaInicial.' '. '06:00:00';
    $fechaFinal = $_GET['fechaFinalVentas'];

    date_default_timezone_set("America/Bogota" ) ;
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinal);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaFinalVentas = $fechaVentasU.' '. '04:00:00';
  /**---------sacando todo lo que se a gastado-------------- */
    $gastosEmpresa = $con->getGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    /**---------sacando el total de los gastos------------------- */
    $gastosTotales = $con->getTotalGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    /**-----------sacando las entradas -------------------- */
    $entradaTotal = $con->getEntradasDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    /**-----------------sacando las ganancias o utilidad entrada menos salida--------------------- */
    $utilidad = $con->getUtilidadDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);

    foreach ($utilidad as $utilidadGatos) {
        /** saca todos los gastos y entradas que se han echo */
        $totalEntrada = $utilidadGatos['utilidad'];
    }

    /**-------------------sacando las ventas totales---de la fecha que queremos saber--------------------------- */
    $ventasTotalByDia = $con->getVentasTotalesDia($fechaInicialVentas, $fechaFinalVentas);
    foreach ($ventasTotalByDia as $totalVentas) {
        $totalVendido = $totalVentas['totalVentas'];
    }
    $utilidadNetaDeLaEmpresa=$totalEntrada + $totalVendido;
    require('../Views/ReporteUtilidad.php');

}
/**--------------Controlador para reporte de gastos ----------------------------*/

if (isset($_GET['gastos'])) {
  /** fechas   asignadas desde la vista por medio de los calendarios */
    $fechaInicial = $_GET['fechaInicialVentas'];
    $fechaInicialVentas =  $fechaInicial.' '. '06:00:00';
    $fechaFinal = $_GET['fechaFinalVentas'];

    date_default_timezone_set("America/Bogota" ) ;
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinal);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaFinalVentas = $fechaVentasU.' '. '04:00:00';
    /**  sacando todos los gastos  */
    $gastosEmpresa = $con->getGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    /** sacando el total de los gastos */
    $gastosTotales = $con->getTotalGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    /**  sacando las entradas de dinero*/
    $entradaTotal = $con->getEntradasDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    require('../Views/ReporteGastosDeLaEmpresa.php');

}
/**--------------Controlador para reporte por año----------------------------*/
if (isset($_GET['reporte_anual'])) {
    $anio = $_GET['anio'];
    /**----------muestra el año------------------------- */
    $ventasMensuales = $con->getVentasMensuales();
    /**---------muestra el mes   y alfrende la suma de venta por mes-------------------- */
    $sumVentasByMes = $con->getTotalVentasByYear($anio);
    /**-------muestra la suma total de ventas de todos los meses  durante el año  es el texto negro grande debajo de la tabla-------------------- */
    $totalVentasMensual = $con->getTotalVentasByAnio($anio);
    require('../Views/ReporteVentasPorAnio.php');
}
/** ----------------------------------Reporte de 6 meses ------------------------------------ */
if (isset($_GET['reporte_6meses'])) {
    $ventasMensuales = $con->getVentasMensuales();
    /** muestra en la vista el listado de meses  y alfrente las ventas */
    $VentasByMes = $con->getTotalVentas6Meses();
    /** muestra en la vista el total de las ventas de los 6 meses esta informacion esta debajo de la tabla */
    $totalVentasMensual = $con->getGrandTotalVentas6Meses();
    require('../Views/ReporteVentasPor6Meses.php');
}


?>