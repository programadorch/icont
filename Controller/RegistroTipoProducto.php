<?php // INGRESA, EDITA Y ELIMINA UN TIPO DE PRODUCTO O CATEGORIA
require('../Model/Conexion.php');
//RUTAS PARA ALMACENAR O EXTRAER IMAGENES 
require('Constans.php');
// VERIFICA SI EL USER INICIO SESSION
if (!isset($_SESSION)) {
    session_start();
}
//VARIABLES PARA MOSTRAR EN LA VISTA EL USUARIO Y LA CONTRASEÑA DE QUIEN EJECUTA UNA ACCION
$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];
//CONEXION CON LA BASE DE DATOS
$con = new conexion();

// ACCIONES QUE SE ACTIVAN CON UN CLIK DEPENDIENDO EL NAME DEL BOTON  AGREGAR NUEVO TIPO PRODUCTO
if (isset($_POST['nuevo_Tipo'])) { //NAME DE EL BOTON EN LA VISTA DE TIPO PRODUCTO
    $tipoproducto = $_POST['tipoProducto']; //NAME DEL IMPUT QUE ALMACENA EL NUEVO TIPO DE PRODCUTO
    // MENSAJE Y COLOR
    $mensaje = "Se registro un nuevo tipo producto  correctamente !!!";
    $alerta = "alert alert-success";
    //CONEXION CON LA BASE DE DATOS PARA ACTUALIZAR LA ALERTA
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //CONEXION CON LA BASE DE DATOS PARA AGREGAR UN NUEVO TIPO
   $regiestroNewTipoProducto = $con->registerNewTipoProduct($tipoproducto);
}

//BORRAR UN TIPO DE PRODUCTO
if (isset($_GET['idborrar'])) { //NAME DE EL BOTON EN LA VISTA DE TIPO PRODUCTO
    //VARIABLES QUE ALMACENAN INFORMACION Y MUESTRAN EN LA VITA QUIEN BORRO EL TIPO DE PRODUCTO
    $usuarioLogin = $_GET['usuarioLogin']; 
    $passwordLogin = $_GET['passwordLogin'];
    $idborrar = $_GET['idborrar'];
    // MENSAJE DE ALERTA Y CARACTERISTICAS
    $mensaje = "Se elimino  los datos del tipo producto correctamente !!!";
    $alerta = "alert alert-danger";
     //CONEXION CON LA BASE DE DATOS PARA ACTUALIZAR LA ALERTA
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
    //CONEXION CON LA BASE DE DATOS PARA eliminar un tipo producto
    $deleteTipoProducto = $con->deleteTipoProduct($idborrar);
}
// EDITAR UN TIPO DE PRODUCTO
if (isset($_POST['update_tipo'])) {
    //VARIABLES QUE ALMACENAN INFORMACION Y MUESTRAN EL ID Y EL TIPO DE PRODUCTO
    $idproducto = $_POST['idtipoproducto']; //NAME DEL IMPUT QUE EDITA EL  ID TIPO DE PRODCUTO
    $tipoproducto = $_POST['tipoProducto'];//NAME DEL IMPUT QUE  EDITA EL   TIPO DE PRODCUTO
    // MENSAJE DE ALERTA Y CARACTERISTICAS
    $mensaje = "Se Actualizo  los datos del tipo Producto correctamente !!!";
    $alerta = "alert alert-info";
    //CONEXION CON LA BASE DE DATOS PARA ACTUALIZAR LA ALERTA
    $updateMensaje = $con->updateMensajeAlert($mensaje, $alerta);
        //CONEXION CON LA BASE DE DATOS PARA editar un tipo producto
    $updateTipoProducto = $con->updateTipoProducto($idproducto, $tipoproducto);
}
//-------------------------------------------------------------------------------
$searchUser = $con->getUser($usuarioLogin, $passwordLogin);
$allUsuarios = $con->getAllUserData();
foreach ($searchUser as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $password = $user['password'];
    $foto = $user['foto'];
}
//----------------------------------------------------------------------------------
$menuMain = $con->getMenuMain();
header("Location: TipoProducto.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");
?>
