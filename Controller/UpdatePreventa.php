<?php  //obtiene los datos de la vista del formulario de editar preventa por eso se usan metodos GET
//nueva catidad actualizada es el valor final de la operacion
// aqui se usan todos los datos que captura el formulario
require('../Model/Conexion.php');
require('Constans.php');

if (!isset($_SESSION)) {
    session_start();
}
//obteniendo datos que se nesecitan para el algotirmo
$idUsuario = $_GET['userId'];

$con = new conexion();

$onlyUserSession = $con->getOnlyUserData($idUsuario);
//atributos con la informacion del usuario 
foreach ($onlyUserSession as $user) {
    $usuario = $user['login'];
    $password = $user['password'];
    $tipoUsuserio = $user['tipo'];
}
$urlViews = URL_VIEWS;
//obtiene los parametros de la Bd mediante el  id del forumlario los rojos son el id los azules son las variables para el echo en el formulario
$idProducto = $_GET['idProducto'];
$imagen = $_GET['imagen'];
$precio = $_GET['precio'];
$precioVenta = $_GET['pventa'];
$tipoUser = $_GET['tipo'];
$tipoPedido = $_GET['tipoPedido'];
$idpreventa = $_GET['idpreventa'];
$nombreProducto = $_GET['producto'];
$cantidadUpdated = $_GET['cantidadUpdated']; //cantidad actualizada
$nuevoPrecio = $_GET['nuevoPrecio']; //precio nuevo de la canrtidad actualizada
// obtiene la cantidad actual  tipo de pedido mesa o llevar 
$getCantidadActual = $con->getCantidadProductoChoose($idProducto, $tipoPedido);
// intercambio de variable
foreach ($getCantidadActual as $cantidadPedidoActual) {
    $cantidadActual = $cantidadPedidoActual['cantidadTotal'];
}
//==================================validaciones para actualizar  las cantidades=========================
//caso 1 si la cantidad acualizada es mayor a la cantidad actual 
if ($cantidadUpdated > $cantidadActual) {
    // c Actualizada =  cantidad escrita en el formulario - la cantidad actual  
    $cantidadNuevaActualizada = $cantidadUpdated - $cantidadActual;
        //recorre 
    for ($i=0; $i <$cantidadNuevaActualizada; $i++ ){
        //inserta la cantidad nueva usando la conexion con base de datos enviando los parametros dentro de las variables azules
        $isertarCantidadActualizada = $con->insertarPreventaProducto($imagen, $nombreProducto, $precioVenta, $idProducto, $precioVenta, $idUsuario, $tipoPedido);
    }
}
// caso 2 si la cantidad actualizada es menor a la cantidad actual y si es distinta de cantidad 1
//   C propuesta es menor a la actual y la propuesta es diferente de 1
if (($cantidadUpdated < $cantidadActual) and ($cantidadUpdated != 1)) {
    // cantidad nueva =  C actual - (Actual -  la propuesta o escrita por teclado)
    $cantidadNuevaActualizada = $cantidadActual - ($cantidadActual - $cantidadUpdated);
        // elimina los datos de la cantidad actual
    $deleteCantidadActual = $con->deleteOnlyPreventa($idProducto,$tipoPedido);
    for ($i=0; $i <$cantidadNuevaActualizada; $i++ ){
        //inserta la cantidad nueva usando la conexion con base de datos enviando los parametros dentro de las variables azules
        $isertarCantidadActualizada = $con->insertarPreventaProducto($imagen, $nombreProducto, $precioVenta, $idProducto, $precioVenta, $idUsuario, $tipoPedido);
    }
}
// caso 3 si la cantidad actualizada es igual a 1 , si el usuario se arrepiente de tantas cantidades y pone que solo quiere 1
//cantidad escrita en teclado
if ($cantidadUpdated == 1) {
    //borra los datos de la cantidad actual 
    $deleteCantidadActual = $con->deleteOnlyPreventa($idProducto,$tipoPedido);
    for ($i=0; $i <$cantidadUpdated; $i++ ){
        //inserta la cantidad nueva usando la conexion con base de datos enviando los parametros dentro de las variables azules
        $isertarCantidadActualizada = $con->insertarPreventaProducto($imagen, $nombreProducto, $precioVenta, $idProducto, $precioVenta, $idUsuario, $tipoPedido);
    }
}
//enviando usuario y contraseña a ventas
header("Location: Ventas.php?usuario=$usuario&password=$password");

?>
