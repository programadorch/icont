<!-- EDITAR LA IMAGEN DE CLIENTES -->
<input id="files" type="file" name="userfileEdit"/>
<output id="list-miniaturaEdit"></output>
<output id="list-datosEdit"></output>


<script>
    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // Objeto de lista de archivos
        // files es una lista de archivos de objetos de archivo. Enumere algunas propiedades.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            // Cierre para capturar la información del archivo.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Renderizar miniatura
                    var span = document.createElement('span');
                    span.innerHTML = ['Nombre: ', escape(theFile.name), ' || Tamanio: ', escape(theFile.size), ' bytes || type: ', escape(theFile.type), '<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniaturaEdit').insertBefore(span, null);
                };
            })(f);

            // Leer en el archivo de imagen como una URL de datos.
            reader.readAsDataURL(f);
        }
        document.getElementById('list-datosEdit').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; //Muestre explícitamente que esto es una copia.
    }

    // Configurar las oyentes dnd.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
</script>

<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Recorra la lista de archivos y renderice archivos de imagen como miniaturas.
        for (var i = 0, f; f = files[i]; i++) {

            // Procesar solo archivos de imagen.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Cierre para capturar la información del archivo.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Renderizar miniatura.
                    var span = document.createElement('span');
                    span.innerHTML = ['<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '" style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniaturaEdit').insertBefore(span, null);
                };
            })(f);

            // Leer en el archivo de imagen como una URL de datos.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>
