<!DOCTYPE html>
<html lang="en">
<?php include('Head.php');
?>

<body>
    <section id="container" class="">
        <header class="header green-bg">
            <!-- AQUI PODEMOS CAMBIAR EL COLOR DEL ENCABEZADO -->
            <div class="toggle-nav">
                <!--TeXTO EMERGENTE EN ENCABEZADO -->
                <!-- ESTILOS DEL ICONO  ICONO DE MENU -->
                <div class="icon-reorder tooltips" data-original-title="Menù de navegaciòn" data-placement="bottom">
                    <i class="icon_menu"></i>
                </div>
            </div>
            <!-- hace el llamado del logo DEL ENCABEZADO -->
            <?php include("Logo.php") ?>
            <!-- casilla de busqueda  -->
            <div class="nav search-row " id="top_menu">
                <ul class="nav top-menu">
                    <li>
                        <form class=" nnavbar-form">
                            <!-- <input class="form-control" placeholder="Buscar..." type="text"> -->
                        </form>
                    </li>
                </ul>
            </div>
            <?PHP include("DropDown.php"); //incluyendo imagen y nombre de inicio login en encabezado azul, TAMBIEN ESTA EL MENU EMERGENTE DE LA FOTO AQUI 
            ?>
        </header>
        <?php include("Menu.php"); ?>
        <!-- Incluyendo el menu izquierdo -->
    </section>
    <!--CONTENIDO DE EL DASH BORAD  O CONDENIDO DE BODY--------------------------------------------------------------->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--overview start-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- PRINCIPAL E INICIO SON  LOS ENLACES DEL NAVEGADOR GRIS -->
                    <h3 class="page-header"><i class="fa fa-laptop"></i> PRINCIPAL</h3>
                    <!-- ALERTA  de USUARIOS ----------------------------------!!!!!!!!!!!!!!!!!!------------------------>
                    <div class="<?PHP echo $alerta; ?>" role="alert"><!-- ESTO PERMITE QUE SEA DINAMICA LA ALERTA SEGUN LO QUE ESTE EN LA BASE DE DATOS -->
                        <strong><?PHP echo $mensaje; ?></strong> 
                    </div>
                    <!-- Menu de inicio arriba de las tarjetas -->
                    <ol class="breadcrumb">
                        <?php include ("MenuOpcionesConfiguracion.php") ?>
                    </ol>
                </div>
            </div>
            <!------------------------------------------- formulario PANTALLA PRINCIPAL  que muestra los usuarios o roles  ------------------------------------>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <section class="panel">
                            <header class="panel-heading">Lista de Usuarios del Sistema</header>
                            <header class="panel-heading">
                                <div clas="panel-body">
                                     <div align="right">
                                        <!-- Boton para activar MODAL agregar nuevos usuarios ---------------------------------------------->
                                        <button href="#addUser" title="" data-placement="left" data-toggle="modal" class="btn btn-warning tooltips" type="button" data-original-title="Nuevo Rol">
                                            <span class="fa da-plus"> </span>
                                            Agregar Nuevo Usuario
                                        </button>
                                    </div>
                                    <!--div para la accion de aparecer modal formulario de agregar usuario---------------------------------------------- -->
                                    <div id="addUser" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <!--Formulario del modal con sus componentes -->
                                        <form class="form-validate form-horizontal" name="form2" action="Registros.php" method="POST" enctype="multipart/form-data">
                                             <!--usuarioLogin y passwordLogin son datos que se almacenan en Registro.php  -->
                                             <input name="usuarioLogin" value="<?PHP echo $usuario; ?>" type="hidden">
                                             <input name="passwordLogin" value="<?PHP echo $password; ?>" type="hidden">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <!--boton para cerrar el modal -->
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                            X
                                                        </button>
                                                        <h3 id=myModalLabel aling="center">Registrar Nuevo Usuario</h3>
                                                    </div>
                                                    <!--Agregar foto de Usuario MODAL-->
                                                    <div class="modal-body">
                                                        <section class="panel" class="col-lg-6">
                                                            <div>
                                                                <strong>
                                                                    Agregar Foto de Usuario
                                                                </strong>
                                                            </div>
                                                            <!--Carga de imagenes Miniatura -->
                                                             <?php include("UploadViewImagecreate.php") ?>
                                                        </section>
                                                        <label for="nombre" class="control-label col-lg-2">Nombre:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="nombre" name="nombre" minlength= "5" type="text" required>
                                                        </div>
                                                        <br></br>
                                                        <!-- CHECK LIST DE ROLES DE USUARIO-->
                                                        <label for="tipo" class="control-label col-lg-2">Tipo:</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-cotrol input-lg m-bot15" name="tipo">
                                                                <option value="ADMINISTRADOR"> ADMINISTRADOR </option>
                                                                <option value="VENTAS"> VENDEDOR </option>
                                                            </select>
                                                        </div>
                                                        <br></br>
                                                    </div>                                                  
                                                    <!--CAJAS DE TEXTO -->
                                                    <label for="login" class="control-label col-lg-2">Apodo:</label>
                                                    <div class="col-lg-10">
                                                        <input class="form-control input-lg m-bot15" id="login" name="login" minlength= " " type="text" placeholder="Con este Apodo accederas a tu cuenta" required>
                                                    </div>
                                                    <br></br>
                                                    <label for="password" class="control-label col-lg-2">Contraseña:</label>
                                                    <div class="col-lg-10">
                                                        <input class="form-control input-lg m-bot15" id="password" name="password" minlength= " " type="text" required placeholder="Con esta contraseña accederas a tu cuenta">
                                                    </div>
                                                    <br>
                                                    <!-- BOTONES DEL FINAL DEL MODAL -->
                                                    <div class="modal-footer">
                                                        <button class = "btn btn-danger" data-dismiss="modal" aria-hidden="true">
                                                                <strong>Cerrar</strong>
                                                        </button> 
                                                        <button name="nuevo_usuario" type="submit" class="btn btn-primary">
                                                                <strong>Registrar</strong>
                                                        </button>
                                                    </div>
                                                </div>                                               
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </header>
                        </section>
                    </div>
                </div>                
            </div>            
            </header>
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <!-- iconos de boostrap  PARA EL FORMULARIO-->
                                <th><i class="icon_images"></i> IMAGEN</th>
                                <th><i class="icon_contacts"></i> NOMBRE</th>
                                <th><i class="icon_folder"></i> TIPO</th>
                                <th><i class="icon_contacts_alt"></i> LOGIN</th>
                                <th><i class="icon_key"></i> CONTRASEÑA</th>
                                <th><i class="icon_cog"> </i> ACCIONES </th>
                            </tr>
                        </thead>
                        <?PHP // recorrer todos los usuarios de la BD  $allUsuarios----------------------------------------------------------
                        while ($datosUsuarios = mysqli_fetch_array($allUsuarios)) {
                        ?>
                            <tr>
                                <th> <img src="<?php echo $urlViews . $datosUsuarios['foto'] ?>  " height="50" width="50"></th>
                                <td> <?PHP echo $datosUsuarios['nombre']; ?></td>
                                <td> <?PHP echo $datosUsuarios['tipo']; ?></td>
                                <td> <?PHP echo $datosUsuarios['login']; ?></td>
                                <td> <?PHP echo $datosUsuarios['password']; ?></td>
                                <td> <!-- botones de eliminar o confirmar Usuarios nuevos, se esta enviando a registros el id el login y el pasword de quien esta eliminando el registro -->
                                <a href="#a<?php echo $datosUsuarios[0]; ?>" role="button" class="btn btn-success" data-toggle="modal">
                                    <i class="icon_check_alt2"></i> </a>
                                <a href="Registros.php?idborrar=<?PHP echo $datosUsuarios[0]; ?>&usuarioLogin=<?PHP echo $usuario; ?>&passwordLogin=<?PHP echo $password; ?>"
                                    role="button" class="btn btn-danger"> <i class="icon_close_alt2"></i>
                                </a>
                                </td>
                            </tr>
                            <!-- EDITAR USUARIO REGISTRADO BOTON EDITAR BACKEND  -->
                            <div id="a<?php echo $datosUsuarios[0]; ?>" class="modal fade" tabindex="-1"
                                             role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form class="form-validate form-horizontal" name="form2"
                                                  action="Registros.php" method="post" enctype="multipart/form-data">
                                                <input name="usuarioLogin" value="<?php echo $usuario; ?>"
                                                       type="hidden">
                                                <input name="passwordLogin" value="<?php echo $password; ?>"
                                                       type="hidden">
                                                <input type="hidden" name="idUsuario"
                                                       value="<?php echo $datosUsuarios['id_usu']; ?>">
                                                <input type="hidden" name="imagen"
                                                       value="<?php echo $datosUsuarios['foto']; ?>">

                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-hidden="true">×
                                                            </button>
                                                            <h3 id="myModalLabel" align="center">Cambiar Informacion del Usuario</h3>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="<?PHP echo $urlViews;
                                                            echo $datosUsuarios['foto']; ?>" width="250" height="250">
                                                            <br><br>
                                                            <section class="panel" class="col-lg-6">
                                                                <div><strong>Cambiar Imagen de usuario</strong></div>
                                                                <?php include("UploadViewImageEdit.php"); ?>
                                                            </section>

                                                            <div class="form-group ">
                                                                <label for="proveedor"
                                                                       class="control-label col-lg-2">Nombre:</label>
                                                                <div class="col-lg-10">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           type="text" name="nombre"
                                                                           value="<?php echo $datosUsuarios['nombre']; ?>">
                                                                    <input type="hidden" name="idUsuario"
                                                                           value="<?php echo $datosUsuarios['id_usu']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="responsable"
                                                                       class="control-label col-lg-2">Tipo:</label>
                                                                <div class="col-lg-10">
                                                                    <select class="form-control input-lg m-bot15"
                                                                            name="tipo">
                                                                        <option value="<?php echo $datosUsuarios['tipo']; ?>"><?php echo $datosUsuarios['tipo']; ?></option>
                                                                        <option value="ADMINISTRADOR">
                                                                            ADMINISTRADOR
                                                                        </option>
                                                                        <option value="VENTAS">VENTAS</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="direccion"
                                                                       class="control-label col-lg-2">Login:</label>
                                                                <div class="col-lg-10">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           type="text" name="login"
                                                                           value="<?php echo $datosUsuarios['login']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group ">
                                                                <label for="telefono"
                                                                       class="control-label col-lg-2">Password:</label>
                                                                <div class="col-lg-10">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           type="text" name="password"
                                                                           value="<?php echo $datosUsuarios['password']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-default" data-dismiss="modal"
                                                                        aria-hidden="true"><strong>Cerrar</strong>
                                                                </button>
                                                                <button name="update_usuario" type="submit"
                                                                        class="btn btn-primary"><strong>Actualizar  Datos</strong></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </section> 
              
    </section>
    <!------------------------------------------- FIN DE formulario que muestra los productos ------------------------------------>
        <?PHP include("LibraryJs.php"); //libreria js para funcionamiento de menu emergente de encabezado    
          ?> 
  
</body>
</html>