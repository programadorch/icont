<!DOCTYPE html>
<html lang="en">
<?PHP
include('LiteralMoney.php');
require_once('Codigo_control.class.php');
error_reporting(0);
?>
<!-- Vista  que muestra la informacion capturadade la Factura -->
<!DOCTYPE html>
<html lang="en">
                        <!-- mostrando los datos de encabezado de factura por medio de las variables que tienen los datos del foreach en el controlador  Registrar preventa-->
                        <table align="left" bgcolor="#ffffff" class="breadcrumb">

                            <tbody>
                            <tr>
                                <td colspan="6" align="Center">
                                    <span style="color: #2b2b2b;">
                                    <?PHP
                                    /* echo es para mostrar */
                                    echo '
                                    <span style="font-size: x-large; "> <div style="text-align: center;"> <strong>'.$razon.'</strong>  </div></span>';
                                    echo '<div style="text-align: center;">De: '.$propietario.' </div> ';
                                    echo '<div style="text-align: center;">Casa Matriz: '.$direccion.'</div> ';
                                    echo '<div style="text-align: center;">   N&ordm; '.$nro.' - Telefono : '.$telefono.'  </div> ';
                                    echo '<div style="text-align: center;">  COCHABAMBA</div> ';
                                    echo '<div style="text-align: center;">  FACTURA ORIGINAL</div> ';
                                    echo ' -----------------------------------------------------------------------------------';
                                    echo '<br>';
                                    ?>
                                   </span>
                                </td>
                            </tr>
                        <!-- mostrando las caracteristicas de la factura   datos legales de la factura,  muestra con el echo por medio del controlador con el  foreach del controlador -registrar preventa--->
                            <tr>

                                <td colspan="3"><span style="color: #2b2b2b;"><b>NIT : </b></span></td>
                                <td colspan="3"><span style="color: #2b2b2b;"><?PHP echo $nit?></span></td>
                            </tr>
                            <tr>
                                <td colspan="3"><span style="color: #2b2b2b;"><b>N&ordm; FACTURA : </b></span></td>
                                <td colspan="3"><span style="color: #2b2b2b;"><?PHP echo $factura?></span></td>
                            </tr>
                            <!--   est numero es para factyrar con la dian 
                            https://actualicese.com/autorizacion-de-numeracion-de-facturacion-en-que-consiste/#:~:text=La%20autorizaci%C3%B3n%20de%20numeraci%C3%B3n%20consiste,facturaci%C3%B3n%20es%20de%20dos%20a%C3%B1os.
                            <tr>
                                <td colspan="3"><span style="color: #2b2b2b;"><b>N&ordm; AUTORIZACION : </b></span></td>
                                <td colspan="3"><span style="color: #2b2b2b;"><?PHP echo $autorizacion?></span></td>
                            </tr> -->
                            <tr>
                                <td colspan="6" align="center"><span style="color: #2b2b2b;"> Otros servicios de comidas</span></td>
                            </tr>

                            <tr>

                                <td colspan="6" align="center">
                                    <span style="color: #2b2b2b;">    -----------------------------------------------------------------------------------</span>
                                </td>
                            </tr>

                            <!-- Mostrando los datos de dia ,  muestra con el echo por medio del controlador con el  foreach del controlador -registrar preventa--------->
                            <tr>

                                <td colspan="6"><span style="color: #2b2b2b;"><b>Fecha de Venta : </b> <?PHP echo $fecha; ?></span></td>
                            </tr>
                            <tr>
                                <td colspan="6"><span style="color: #2b2b2b;"><b>Nit/CI : </b> <?PHP echo $ci; ?></span></td>
                            </tr>
                            <tr>
                                <td colspan="6"><span style="color: #2b2b2b;"><b>Se&ntilde;or(es): </b> <?PHP echo $nombreCliente; ?> <?PHP echo $apellido; ?></span></td>
                            </tr>

                            <tr>

                                <td colspan="6" align="center">
                                    <span style="color: #2b2b2b;">    -----------------------------------------------------------------------------------</span>
                                </td>
                            </tr>
                            <!--Mostrando Las cantidades de producto y total ,  muestra con el echo por medio del controlador con el  foreach del controlador -registrar preventa--->
                            <tr>
                                <td width="10"></td>
                                <td width="40"><span style="color: #2b2b2b;"><b>Cant.</b></span></td>
                                <td width="100"><span style="color: #2b2b2b;"><b>Descripcion</b></span></td>
                                <td width="70"><span style="color: #2b2b2b;"><b>Precio</b></span></td>
                                <td width="70"><span style="color: #2b2b2b;"><b>Total</b></span></td>
                                <td width="70"><span style="color: #2b2b2b;"><b>Tipo</b></span></td>
                            </tr>
                            <?PHP
                           /* aqui es donde hace el recorrido de las filas  */
                            for ($i=0; $i<$pedido; $i++){
                                echo '<tr>';
                                /* $pedidoTotalPreventa esta definida en controlador registrarpreventa.php */
                                $detallePedido = mysqli_fetch_array($pedidoTotalPreventa);
                                //obteniendo el valor de los atributos para mostrar
                                echo '<td>'.'</td>';
                                echo '<td width="40"><span style="color: #2b2b2b;">'.$detallePedido['cantidad'].'</span></td>';
                                echo '<td width="100"><span style="color: #2b2b2b;">'.$detallePedido['producto'].'</span></td>';
                                echo '<td width="70"><span style="color: #2b2b2b;">'.$detallePedido['precio'].'</span></td>';
                                // aqui hace la operacion matematica ----------------------------
                                echo '<td width="70"><span style="color: #2b2b2b;">'.$detallePedido['precio']*$detallePedido['cantidad'].'</span></td>';
                                echo '<td width="70"><span style="color: #2b2b2b;">'.$detallePedido['tipo'].'</span></td>';
                                echo '</tr>';
                              }


                            ?>
                                <!--=============== Mostrando el valor de de la factura impuestos y cambio===== variables de claradas en el controlador regristrar preventa.php=======-->
                            <tr>

                                <td colspan="6" align="center">
                                    <span style="color: #2b2b2b;">    -----------------------------------------------------------------------------------</span>
                                </td>
                            </tr>
                                <!-- mostrando los totales de prescios   muestra con el echo por medio del controlador con el  foreach del controlador -registrar preventa--->
                            <tr>
                                <!-- tipo de moneda esta primero que las demas variables para mostrar el simbolo-->
                                <td colspan="6"><span style="color: #2b2b2b;"><b>Total a Pagar : <?PHP echo $tipoMoneda; ?>  </b> <?PHP echo $totalAPagar; ?></span></td>
                            </tr>
                            <tr>
                                <td colspan="6"><span style="color: #2b2b2b;"><b>Efectivo : <?PHP echo $tipoMoneda; ?> </b> <?PHP echo $efectivo; ?></span></td>
                            </tr>
                            <tr>
                                <td colspan="6"><span style="color: #2b2b2b;"><b>Cambio : <?PHP echo $tipoMoneda; ?> </b> <?PHP echo $cambio; ?></span></td>
                            </tr>
                            
                            <tr>
                                <td colspan="6">&nbsp;<span style="color: #2b2b2b;"> <b>Son : </b>
                                <?PHP  /* mostran valor en Letras */
                                 $literalMoney = new  EnLetras();
                                  $shoLiteralMoney = strtoupper($literalMoney -> valorEnletras($totalAPagar,$contextMoneda));
                                  echo "<b>".$shoLiteralMoney. "</b>";
                                  ?>
                                      </span>
                                </td>
                            </tr>
                            <!-- CODIGO DE CONTROL -->
                            <tr>
                                <td colspan="6"><span style="color: #2b2b2b;"><b>Codigo de control : <?PHP echo $getCodigoControl; ?></b></span></td>
                            </tr>
                            <!--  fecha de emision  -->
                            <tr>
                                <td colspan="6"><span style="color: #2b2b2b;"><b>Fecha Limite de Emision :  <?PHP echo $fechaLimiteEmision; ?></b></span></td>
                            </tr>
                             <!-- llamando el codigo QR -->
                             
                            <tr>
                                <td colspan="6" align="center"><b>
                                    <?PHP  include ("Qr.php"); ?>
                                    </b></td>
                            </tr> 

                            <tr>

                                <td colspan="6" align="center">
                                    <span style="color: #2b2b2b;">    -----------------------------------------------------------------------------------</span>
                                </td>
                            </tr>


                            <tr>
                                <td colspan="6" align="center"><span style="color: #2b2b2b;"> " Esta factura conttribuye al desarrollo del pais el uso
                                    ilicito de esta sera sancionado de acuerdo a ley "</span>
                                </td>
                            </tr>

                            <tr>

                                <td colspan="6" align="center">
                                    <span style="color: #2b2b2b;">    -----------------------------------------------------------------------------------</span>
                                </td>
                            </tr>
                            <!-- variables que estan en el controlador registrar prevente.php -->
                            <tr>
                                <td colspan="4">
                                  <span style="color: #2b2b2b;">  Atendido por : <strong><?PHP echo $userLogueado;?> </strong></span>
                                </td>
                              
                                <td colspan="2">
                                    <span style="color: #2b2b2b;">  FICHA NRO :<strong> <?PHP echo  $ficha; ?> </strong> </span>
                                </td>
                            
                            </tr>
                            </tbody>
                        </table>


                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-4"></div>


