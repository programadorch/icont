<!-- vista que captura la informacion  de la factura -->

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <!-- calculadora es el nombre del formulario  y en  input efectivo value = onkeyup realice la  suma -->
            <!--tiene el metodo post para enviar el ci y otras cosas -->
            <form class="form-validate form-horizontal" name="calculadora" action="RegistrarPreventa.php" method="post"> 
                <!-- ESTE ID ES MUY IMPORTANTE PARA HACER LA OPERACIONES MATEMATICAS-->               
                <table id="dataTables-example">
                    <!-- mandamos el usuario que esta haciendo la trasaccion / eso se envia al controlador factura.php con estos datos podemos hacer informes -->
                    <input type="hidden" id="usuario" name="usuario" value="<?php echo $usuario; ?>">
                    <input type="hidden" id="password" name="password" value="<?php echo $password; ?>">
                    <!-- boton Registrar -->
                    <tr>
                        <th colspan="5" align="Center"> REGISTRAR</th>
                        <th>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        </th>
                    </tr>
                    <!-- imputs  / CAJAS DE TEXTO  ------------------------------------->
                    <tr>
                        <td>
                            <label>
                                DOCUMENTO :
                            </label>
                        </td>
                        <td colspan="7">
                            <div class="col-lg-10">
                               <!--el ci es el mismo que se declara en el controlador registrarpreventa por medio de un post por que el formulario tiene esa funcion --> 
                            <!-- class="form-control input-lg m-bot15"    ESTiLO ADAPTABLE PARA IMPUT    myFunctionSearch es para buscar a los clientes-->
                                <input class="form-control input-lg m-bot15" type="text" 
                                 required name="ci" autocomplete="off"  onkeyup="myFunctionSearch()" id="searchNit"  >
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>
                                NOMBRE :
                            </label>
                        </td>
                        <td colspan="7">
                            <div class="col-lg-10">
                                <div  class="textbox2" size="25" readonly  name="nombreCliente" id="myDiv"></div>
                            </div>
                        </td>
                    </tr>
                    <!--  onKeyUp="Suma"  esta en los 3 imputs por que son los que van a interactuar cada 1 tiene un nombre diferente ingreso 1 2 y RESULTADO -->
                    <tr>
                        <td><label>TOTAL A PAGAR :</label></td>
                        <td>
                            <div class="col-lg-20">
                                <!--EN ESTE IMPUT SE OBSERVA  EL VALOR TOTAL DE LA PREVENTA  echo $preventa;-->
                                <input class="form-control input-lg m-bot15" type="text"  required name="ingreso1"
                                       readonly value="<?PHP echo $preventa; ?>" onKeyUp="Suma()">

                            </div>
                        </td>


                        <td><label>EFECTIVO :</label></td>
                        <td>
                            <div class="col-lg-20">
                                <!--El evento onkeyup ocurre cuando el usuario suelta una tecla "en el teclado". -->
                                <!-- onKeyUp="Suma" es la funcion que realizara la  operacion de total de vueltods -->
                                <input class="form-control input-lg m-bot15" type="text"  required name="ingreso2"
                                       value="" onKeyUp="Suma()">

                            </div>
                        </td>


                        
                    </tr>
                    <!-- cambio esta en un tr aparte para poder ver los valores que tienen mas de 3 cifras completos en pantalla-->
                    <tr>
                    <td><label>CAMBIO :</label></td>
                        <td>
                            <div class="col-lg-20">
                                <input class="form-control input-lg m-bot15" type="text"  required name="resultado"
                                       readonly onKeyUp="Suma()">

                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="5"></td>
                        <td colspan="1">
                            <button name="registrar" type="submit" class="btn btn-success"> ACEPTAR</button>
                        </td>
                    </tr>


                </table>
            </form>

        </div>
    </div>
</div>

<!-- FUNCION QUE HACE LAS OPERACIONES DE LA SUMA -->
<script>
   // EL BOTON REGISTRAR permanece desabililitado
    document.calculadora.registrar.disabled=true;

    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

    // =====================================FUNCION PARA BUSCAR EL DOCUMENTO DEL CLIENTE Y ARASTRAR EL NOMBRE ======================
    function myFunctionSearch() {

        var clientSearch = document.getElementById('searchNit').value;  // VARIABLE  clientSearch = searchNit ID DEL IMPUT DOCUMENTO
    //valida si esta vacia la busqueda
        if (clientSearch == '') {  // SIE L VALOR DE ESE ID ES VACIO  
            document.getElementById("myDiv").innerHTML = "";  //myDiv ES EL ID DEL IMPUT NOMBRE 
            document.getElementById("myDiv").style.border = "0px";  //
            document.getElementById("pers").innerHTML = "";
            return;
        }
        //INTERACTUAR de forma dinamica   si busca el documento y existe va amostrar el nombre y se va a pintar el imput
        loadDoc("nitClient=" + clientSearch, "SearchContact.php", function () {

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { // xmlhttp variable que se encuentra en ajax
                document.getElementById("myDiv").innerHTML = xmlhttp.responseText; // se renderiza el nombre o actualiza nombre
                document.getElementById("myDiv").style.border = "1px solid #A5ACB2"; // se renderiza estilo de imput

            }

        });
    }

  // FUNCION PARA LA OPERACION DE PAGOS-----------------------------------------------------------------------------------------------
    function Suma() {
        //VARIABLES PARA OBTENER LOS DATOS DE LOS IMPUTS
        var ingreso1 = document.calculadora.ingreso1.value; //NOMBRES DE LOS IMPUTS / OPTIENIENDO LOS DATOS INGRESO 1 TOTAL A PAGAR 
        var ingreso2 = document.calculadora.ingreso2.value; //NOMBRES DE LOS IMPUTS / OPTIENIENDO LOS DATOS INGRESO 2 EFECTIVO
        try {
            ingreso1 = (isNaN(parseFloat(ingreso1))) ? 0 : parseFloat(ingreso1); // INGRESO 1 ES DISTINTO DE 0
            ingreso2 = (isNaN(parseFloat(ingreso2))) ? 0 : parseFloat(ingreso2);
            document.calculadora.resultado.value = ingreso2 - ingreso1;  // RESULTADO DE LA OPERACION =OPERACION MATEMATICA  TOTAL A PAGAR - EFECTIVO 

            if ((ingreso2 - ingreso1) >= 0) {  //SI EFECTIVO - TOTAL A PAGAR ES MAYOR O IGUA A 0
                document.calculadora.registrar.disabled = false;  //se activa el boton  RTEGISTRO
            }

            if((ingreso2 == "") || (document.calculadora.resultado.value < 0) ){  //SI EFECTIVO ES VACIO 
                document.calculadora.registrar.disabled=true; //sigue bloqueado el boton
            }
        } catch (e) {

        }

    }

</script>
