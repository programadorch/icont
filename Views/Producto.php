<?php  //====================CARGUE DE PRODUCTOS PARA LA VISTA DE VENTAS=================
$row = 1;
while ($product = mysqli_fetch_array($allProducto)) { //recorre la lista de todos los productos
    if ($row > 4) { //> 4 columnas
        echo "</tr><tr class='success'>"; 
        $row = 1; 
    }
    ?>
    <td background="<?PHP echo $urlViews; ?>img/menuPOS.jpg" align="center"> <!-- img/menuPOS.jpg  imagen de fondo del producto miniatura -->
        <div style="width: 112px">
            <div class="single-product">
                <div class="product-f-image">
                    <!-- MUESTRA DE IMAGEN DE PRODUCTO Y SUS DIMENCIONES -->
                    <img src="<?PHP echo $urlViews . $product['imagen']; ?>" width="90" height="90" class="imgRedonda"> <!--imagen de productos -->
                    <div class="product-hover">
                        <!-- BOTONES DE TOMAR PEDIDOS   Y SUS ESTILOS Y ACCIONES "envian la orden al controlador AJAXPOS.JS" CON RATON              id de producto y id de usuario pa saber quin vendio-->
                        <a onclick="insertarPedidoMesa('<?PHP echo $product['idproducto'];?>','<?PHP echo $id_usuario;?>')" data-name="Mouse" style="text-decoration: none; cursor: pointer;"
                           class="add-to-cart-link">Mesa</a>
                        <a onclick="insertarPedidoLlevar('<?PHP echo $product['idproducto'];?>','<?PHP echo $id_usuario;?>')" data-name="Mouse" style="text-decoration: none; cursor: pointer;"
                           class="view-details-link">Llevar</a>
                    </div>
                        <!-- MUESTRA CARACTERISTICAS DEL PRODUCTO -->
                    <span style="color: #FFFFFF">                    
                    <b>
                        <?PHP echo $product['nombreProducto'];
                        echo '<br>';
                        echo $product['precioVenta'];
                        echo '&nbsp;';
                        echo $tipoMonedaElegida; ?>   .
                    </b>
                        </span>
                </div>
            </div>
        </div>
    </td>
    <?php
    $row++; //aumenta las filas
}

echo '</tr>';


?>