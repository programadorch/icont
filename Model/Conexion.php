<?php
class Conexion {
    //declaracion de variables privadas
    private $user;//usuario
    private $password;///contraseña
    private $server; //localhost
    private $database;//base de datos
    private $con;//conexion
    //funcion 1 para asignar parametros alas variables 
    public function __construct()
    {
        $user = 'root';
        $password = '';
        $server = 'localhost';//aquo podria ir los cambios para el servidor web
        $database = 'icontpos';//nombre de la base de datos
        $this->con = new mysqli($server, $user, $password, $database);//enviamos parametros a la nueva  conexion 
    }
     //esta funcion 2 esta relacionada en AccessUsers linea 16
    // funcion publica  que optiene un usuario que recibe 2 parametros el user y pasword, donde se crea una consulta -----------------------
    public function getUser($usuario, $password){
            //consulta para buscar y extraer los valores del formulario un usuario y un pasword, osea coincidencias de datos
            $query = $this->con->query("SELECT * FROM usuarios WHERE login='" . $usuario . "' AND password='" . $password . "'");
            //y guarde en el vector retono lo que encuentre
            $retorno = [];

            $i = 0;
            while ($fila = $query->fetch_assoc()) {
                $retorno[$i] = $fila;
                $i++;
            }
            return $retorno;
    }
    // FUNCION 3 PARA HACER LA CONSULTA DEL MENU  PRINCIPAL DE ADMINITRACION --------------------------------------------------------------------
    public function getMenuMain(){
        //consulta para buscar y extraer los valores del formulario un usuario y un pasword, osea coincidencias de datos
        $query = $this->con->query("SELECT * FROM `menu`");
        //y guarde en el vector retono lo que encuentre
        $retorno = [];
        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
}
   // FUNCION 4 PARA HACER LA CONSULTA DEL MENU  DE USUARIO VENTAS--------------------------------------------------------------------
   public function getMenuMainVentas(){
    //consulta para buscar y extraer los valores del formulario cuando en la base de datos acceso sea igual a a en tabla menu
    $query = $this->con->query("SELECT * FROM `menu` where acceso ='A'");
    //y guarde en el vector retono lo que encuentre
    $retorno = [];
    $i = 0;
    while ($fila = $query->fetch_assoc()) {
        $retorno[$i] = $fila;
        $i++;
    }
    return $retorno;
}
       //FUNION 5 PARA VER LOS USUARIOS Y ROLES DE LA BASE DE DATOS---------------------------------------------------
 public function getAllUserData()
    {
// funcion publica  que optiene todos los datos de la tabla usuario de la BD
        $query = $this->con->query("SELECT * FROM usuarios ");
 //y guarde en el vector retono lo que encuentre
        return $query;
    }
 // ==============FUNCION para ENVIAR EL ID DEL USUARIO QUE HACE LA ACCION===============================================================================
    public function getOnlyUserData($idUser)
    {

        $query = $this->con->query("SELECT * FROM usuarios where id_usu=$idUser");
        $retorno = [];
        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    } 
    //------------Funcion para   agrupar y mostrar  Atributos nesesarios de la tabla  preventa en una columna llamada total-----------------------
    public function getPreventa()
    {
        $query = $this->con->query("SELECT idPreventa,imagen,producto,COUNT(producto) as cantidad, SUM(precio) as totalPrecio,idProducto,pventa,idUser,precio,tipo 
                                            FROM `preventa` 
                                            GROUP BY producto,idProducto,tipo /* producto,idProducto,tipo*/
                                            ORDER BY idPreventa ASC"); // orden asecndente
        return $query;
    }
    
    // ==================Funcion para mostrar la suma de  los precios de los productos en preventa 
    public function getTotalPreventa()
    {
        $query = $this->con->query("SELECT Sum(precio) as total , idUser FROM `preventa`");
        return $query;
    }

    //FUNION 6 PARA REGISTRAR UN NUEVO USUARIO O ROL------------------------------------------------
 public function getRegisterNewUser($nombre,$tipo,$usuario,$password,$imagenUsuario)
 {
// funcion publica  que optiene todos los datos de la tabla usuario de la BD
//variable que almacena los datos del modal y los almacena directamente a la base de datos $nombre, $tipo,$usuario,$password,$imagenUsuario
$query = $this->con->query("INSERT INTO `usuarios` (`id_usu`, `login`, `tipo`, `nombre`, `password`, `foto`) 
VALUES (NULL, '$usuario', '$tipo', '$nombre', '$password', '$imagenUsuario')");
//y guarde en el vector retono lo que encuentre
         return $query;
     }
     //----------------------7 FUNCION   PARA ELIMINAR USUARIO -----------------------
     public function deleteUsuario($idUsuario)
     {
 
         $query = $this->con->query("DELETE FROM usuarios Where id_usu=$idUsuario ");
 
         return $query;
     }
     //8 funcion para EDITAR USUARIO-------------------------------------------
     public function updateUsuario($login, $tipo, $nombre, $password, $foto, $idUsuario)
     { 
         $query = $this->con->query("UPDATE `usuarios` 
                                           SET `login` = '$login', 
                                                `tipo` = '$tipo', 
                                                 `nombre` = '$nombre',
                                                  `password` = '$password', 
                                                  `foto` = '$foto' WHERE `usuarios`.`id_usu` = $idUsuario");
 
         return $query;
     }
     //--------------------9 Funcion Para mensaje de Alerta en USUARIOS------------------------------------------
     public function getMensajeAlerta()
     { 
         //consulta de la base de datos
         $query = $this->con->query("SELECT * FROM `alerta`"); 
         //DEVUELVE UN OBJETO
         $retorno = []; 
         $i = 0;
         while ($fila = $query->fetch_assoc()) {
             $retorno[$i] = $fila;
             $i++;
         }
         return $retorno;
 
     }
        //-----------------------------10 FUNCION PARA ACTUALIZAR LA ALERTA-------------------------------------------------
     public function updateMensajeAlert($mensaje, $alerta)
     {
         $query = $this->con->query("UPDATE `alerta` SET `tipoAlerta` = '$alerta',
                                                 `mensaje` = '$mensaje'  WHERE `alerta`.`alertaId` = 1");
         return $query;
     }
     // -------------------------------11 Consulta par ver DATOS DE LA FACTURA   envia al controlador datosfactura $dataFactura =$con->getDataFactura();---------------------------------------------
     public function getDataFactura()
     {
         $query = $this->con->query("SELECT * FROM `datos`");
         return $query;
     }
     //-----------------------------------12 FUNCION PARA ACTUALIZAR FACTURA--------------------------------------------------------------------------
     public function updateDataFactura($propietario, $razon, $direccion, $nro, $telefono)
    {

        $query = $this->con->query("UPDATE `datos` SET `propietario` = '$propietario', 
                                                             `razon` = '$razon',
                                                             `direccion` = '$direccion', 
                                                              `nro` = '$nro',
                                                              `telefono` = '$telefono'
                                                               WHERE `datos`.`iddatos` = 1");
        return $query;
    }
    // 13 funcion para ver los tipos de MONEDA de la BD tabla moneda----------------------------------------------------------------------
    public function getMoneda()
    {
        $query = $this->con->query("SELECT * FROM `moneda`");
        return $query;
    }
    // 14 FUNCION PARA ACTUALIZAR EL TIPO DE MONEDA CON EL MODAL-------------------------------------------------------------------------
    public function updateDataMoneda($idMoneda, $pais, $tipoMoneda, $contexto)
    {
        $query = $this->con->query("UPDATE `moneda` SET 
                                          `pais` = '$pais', 
                                          `tipoMoneda` = '$tipoMoneda', 
                                          `contexto` = '$contexto' WHERE `moneda`.`idMoneda` = $idMoneda ");
        return $query;
    }
    // 15 consulta para ver los idiomas existentes en la bd------------------------------------------------------------------
    public function getIdioma()
    {
        $query = $this->con->query("SELECT * FROM `idioma`");
        return $query;
    }
    // 16 funcion para actualizar el idioma en la base de datos-----------------------------------------------------------------
    public function updateDataIdioma($idioma, $idIdioma)
    {

        $query = $this->con->query("UPDATE `idioma`
                                          SET `idioma` = '$idioma' 
                                          WHERE `idioma`.`idIdioma` = $idIdioma");
        return $query;
    }
    //17 FUNCION PARA  ACTUALIZAR IDIOMA EN EL MENU---------ENVIA PARAMETROS A REGISTROSDATAIDIOMA.PHP----------------------------
    public function updateIdiomaSistem($opcionMenu, $idIdioma)
    {
        $query = $this->con->query("UPDATE `menu` 
                                          SET `opcion` = '$opcionMenu' 
                                          WHERE `menu`.`idmenu` = $idIdioma ");
        return $query;
    }
      // funcion para ver todos los proveedores--------------------------------------------------------------------------
    public function getAllProveedor()
    {

        $query = $this->con->query("SELECT * FROM proveedor ");
        return $query;
    }
    //funcion para Registrar o actualizar nuevo proveedor----------------------------------------------------------------
    public function registerNewProveedor($proveedor, $responsable, $direccion, $telefono, $fechaRegistro)
    {

        $query = $this->con->query("INSERT INTO `proveedor` (`idproveedor`, `proveedor`, `responsable`, `fechaRegistro`, `direccion`, `telefono`, `estado`, `fechaAviso`, `valor`, `valorCobrado`, `saldo`) VALUES (NULL, '$proveedor', '$responsable', '$fechaRegistro', '$direccion', '$telefono', '', '', '', '', '') ");
        return $query;
    }
    //Funcion para eliminar UN PROVEEDOR --------------------------------------------------------------------
    public function deleteProveedor($idProveedor)
    {
        $query = $this->con->query("Delete from proveedor where idproveedor=$idProveedor ");
        return $query;
    }
    //FUNCION PARA ACTUALIZAR UN PROVEEDOR 
    public function updateProveedor($idProveedor, $proveedor, $responsable, $direccion, $telefono, $fechaRegistro)
    {
        $query = $this->con->query("UPDATE `proveedor` SET `proveedor` = '$proveedor', 
                                            `responsable` = '$responsable', 
                                            `fechaRegistro` = '$fechaRegistro', 
                                            `direccion` = '$direccion',
                                             `telefono` = '$telefono' WHERE `proveedor`.`idproveedor` = $idProveedor");
        return $query;
    }
    //-------------------------FUNCION PARA MOSTRAR LOS CLIENTES------------------------
    public function getAllCliente()
    {
        $query = $this->con->query("SELECT * FROM cliente ");
        return $query;
    }
    //-----------------------------------Ingresar Un Nuevo Cliente ----------------------------------------------
    public function registerNewCliente($imagen, $nombre, $apellido, $direccion, $telefonoFijo, $telefonoCelular, $email, $fechaRegistro, $ci)
    {

        $query = $this->con->query("INSERT INTO `cliente` (`idcliente`, `foto`, `nombre`, `apellido`, `direccion`, `telefonoFijo`, `telefonoCelular`, `email`, `contactoReferencia`, `telefonoReferencia`, `observaciones`, `fechaRegistro`, `ci`) 
                                     VALUES (NULL, '$imagen', '$nombre', '$apellido', '$direccion', '$telefonoFijo', '$telefonoCelular', '$email', '', '', '', '$fechaRegistro', '$ci')");

        return $query;
    }
    //-----------------------actualizar un cliente------------------------------------------------------------------
    public function updateClient($idcliente, $imagen, $nombre, $apellido, $direccion, $telefonoFijo, $telefonoCelular, $email, $fechaRegistro, $ci)
    {

        $query = $this->con->query("UPDATE `cliente` SET 
                                                `foto` = '$imagen', 
                                                `nombre` = '$nombre', 
                                                `apellido` = '$apellido', 
                                                `direccion` = '$direccion',
                                                `telefonoFijo` = '$telefonoFijo', 
                                                `telefonoCelular` = '$telefonoCelular', 
                                                `email` = '$email', 
                                                `fechaRegistro` = '$fechaRegistro', 
                                                `ci` = '$ci' WHERE `cliente`.`idcliente` = $idcliente");

        return $query;
    }
    //--------------------------ELIMINAR CLIENTE ------------------------------------------------------------------------
    public function deleteClient($idClient)
    {
        $query = $this->con->query("Delete from cliente where idcliente=$idClient ");
        return $query;
    }
    //-------------------------------TIPO DE MONEDA para usar en la vista de VEntas---------------
    public function getTipoMoneda()
    {
        $query = $this->con->query("SELECT * FROM `moneda`");
        $retorno = [];
        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    //------------------------------------Muestra el -Producto elejido en ventas-------------------------------
    public function getProductoElegido($idproducto)
    {

        $query = $this->con->query("SELECT * FROM `producto` where idproducto='$idproducto'");
        $retorno = [];
        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }
    //-------------------------------------------Insertar prosductos a la preventa ----------------------------
    public function insertarPreventaProducto($imagen, $producto, $precio, $idProducto, $pventa, $idUser, $tipo)
    {
        $query = $this->con->query("INSERT INTO `preventa` (`idPreventa`, `imagen`, `producto`, `precio`, `idProducto`, `pventa`, `idUser`, `tipo`)
                                          VALUES (NULL, '$imagen', '$producto', '$precio', '$idProducto', '$pventa', '$idUser', '$tipo')");

        return $query;
    }

    //-----------------------------Muestra los productos en la vistaproductos ------------------------------------------------
    public function getAllProducto()
    {
        $query = $this->con->query("SELECT * FROM producto");
        return $query;
    }
    //------------------------------MUESTRA TODOS LOS TIPOS DE PRODUCTO EN EL MODAL DE AGREGAR PRODUCTO LISTA DESPLEGABLE- O COMBOBOX---------------
    public function getAllTipoProducto()
    {
        $query = $this->con->query("SELECT * FROM tipoproducto");
        return $query;
    }
    //------------------------REGISTRAR UN NUEVO PRODUCTO --------------------------------------------------------
    public function registerNewProducto($imagen, $codigo, $nombreProducto, $cantidad, $fechaRegistro, $precioVenta, $tipo, $proveedor, $precioCompra)
    {

        $query = $this->con->query("INSERT INTO `producto` (`idproducto`, `imagen`, `codigo`, `nombreProducto`, `cantidad`, `fechaRegistro`, `precioVenta`, `tipo`, `proveedor`, `precioCompra`) 
                                          VALUES (NULL, '$imagen', '$codigo', '$nombreProducto', '$cantidad', '$fechaRegistro', '$precioVenta', '$tipo', '$proveedor', '$precioCompra')");
        return $query;
    }
    //----------------------------------BORRAR UN PRODUCTO-----------------------------------------------
    public function deleteProduct($idproducto)
    {
        $query = $this->con->query("Delete from producto where idproducto=$idproducto");
        return $query;
    }
    // -------------------------------ELIMINAR PREVENTA EN LINEA------------------------
    public function deleteOnlyPreventa($idProducto, $tipo)
    {
        $query = $this->con->query("Delete from preventa where idproducto='$idProducto'  and  tipo='$tipo'");
        return $query;
    }
    //====================================ELIMINA TODA LA PREVENTA===============================================
    public function deleteAllPreventa()
    {
        $query = $this->con->query("TRUNCATE `preventa`");
        return $query;
    }

    //---------------------------------ACTUALIZAR PRODUCTO--------------------------------------------------------------
    public function updateProduct($imagen, $codigo, $nombreProducto, $cantidad, $fechaRegistro, $precioVenta, $tipo, $proveedor, $precioCompra, $idproducto)
    {

        $query = $this->con->query("UPDATE `producto` SET `imagen` = '$imagen', 
                                                     `codigo` = '$codigo',
                                                     `nombreProducto` = '$nombreProducto',
                                                     `cantidad` = '$cantidad', 
                                                     `fechaRegistro` = '$fechaRegistro', 
                                                     `precioVenta` = '$precioVenta', 
                                                     `tipo` = '$tipo',
                                                      `proveedor` = '$proveedor', 
                                                      `precioCompra` = '$precioCompra' WHERE `producto`.`idproducto` = $idproducto");
        return $query;
    }
    // ===============================AGREGAR UN NUEVO tipo de PRODUCTO o CATEGORIA---------------------
    public function registerNewTipoProduct($tipoProducto)
    {
        $query = $this->con->query("INSERT INTO `tipoproducto` (`idtipoproducto`, `tipoproducto`) 
                                          VALUES (NULL, '$tipoProducto')");
        return $query;
    }
    //------------------------------ELIMINAR UN NUEVO tipo de PRODUCTO o CATEGORIA----------------------
    public function deleteTipoProduct($tipoProductoId)
    {
        $query = $this->con->query("Delete from tipoproducto where idtipoproducto=$tipoProductoId");
        return $query;
    }
    //-------------------------------ACTUALIZAR TIPO DE PRODUCTO O CATEGORIA----------------------
    public function updateTipoProducto($tipoProductoId, $tipoproducto)
    {
        $query = $this->con->query("UPDATE `tipoproducto` SET `tipoproducto` = '$tipoproducto'
                                          WHERE `tipoproducto`.`idtipoproducto` = $tipoProductoId");
        return $query;
    }
    //---------------------------------------MUESTRA LOS PRODUCTOS DEL INVENTARIO ---------------------------
    public function getAllActivos()
    {
        $query = $this->con->query("SELECT * FROM activos ");
        return $query;
    }
     //-------------------------------------- GUARADA UN NUEVO PRODUCTO  DEL INVENTARIO ---------------------------
    public function registerNewActivo($imagen, $codigo, $nombreProducto, $cantidad, $fechaRegistro)
    {

        $query = $this->con->query("INSERT INTO `activos` (`idactivo`, `imagen`, `codigo`, `nombreProducto`, `cantidad`, `fechaRegistro`) 
                                          VALUES (NULL, '$imagen', '$codigo', '$nombreProducto', '$cantidad', '$fechaRegistro')");

        return $query;
    }
     //-------------------------------------- ELIMINA  PRODUCTO AL  INVENTARIO ---------------------------
    public function deleteActivo($idproducto)
    {
        $query = $this->con->query("Delete from activos where idactivo=$idproducto");

        return $query;
    }
     //--------------------------------------EDITA UN   PRODUCTO DEL  INVENTARIO ---------------------------
    public function updateActivo($imagen, $codigo, $nombreProducto, $cantidad, $fechaRegistro, $idproducto)
    {

        $query = $this->con->query("UPDATE `activos` SET `imagen` = '$imagen', 
                                                     `codigo` = '$codigo',
                                                     `nombreProducto` = '$nombreProducto',
                                                     `cantidad` = '$cantidad', 
                                                     `fechaRegistro` = '$fechaRegistro' 
                                                      WHERE `activos`.`idactivo` = $idproducto");

        return $query;
    }
  //======EDITAR=================OBTIENE EL PRODUCTO ELEGIDO PARA EDITARLO===================
    public function getDataProductoChoose($idProducto, $tipo)
    {
        $query = $this->con->query("SELECT * FROM `preventa` where idproducto='$idProducto' and tipo='$tipo'");
        $retorno = [];
        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

//================================Muestra o selescciona la cantidad del producto que se va a elegir============================
    public function getCantidadProductoChoose($idProducto, $tipo)
    {
        $query = $this->con->query("SELECT count(idproducto) as cantidadTotal FROM `preventa` where idproducto='$idProducto' and tipo='$tipo'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;

    }
    //---------------------------FUNCION PARA MOSTRAR TODOS LOS CLIENTES SI CONINCIDEN CON EL NIT--------------------------
    public function getContact($nitClient)
    {
        $query = $this->con->query("SELECT * FROM `cliente`  where  ci='$nitClient'");
        return $query;
    }
    //--------------------------funcion para mostrar la tabla datos y verla en la factura---estoiado al clador al controlador regsitrar preveta--------------------
    public function getDatosFactura()
    {
        $query = $this->con->query("SELECT * FROM `datos`");
        return $query;
    }
        //-----------------funcion para mostrar los datos legales de la factura  posiblemente aqui van lo datos de la dian llave, nit etc numero de factura
    public function getDatosDosificacion()
    {
        $query = $this->con->query("SELECT * FROM `dosificacion`");
        return $query;
    }
    //----------------------------Funcion para insertar en la tabla Clientedato los atributos caputados por la vista factireviews.php
    public function registrarDatosPreventa($ci, $nombre, $totalAPagar, $efectivo, $cambio, $fechaVenta, $idcliente)
    {
        $query = $this->con->query("INSERT INTO `clientedato` (`idCliente`, `nombre`, `ci`, `fecha`, `totalApagar`, `efectivo`, `cambio`, `idClientei`, `tipoVenta`) 
                                            VALUES (NULL , '$nombre', '$ci', '$fechaVenta', '$totalAPagar', '$efectivo', '$cambio', '$idcliente', 'Local');");
        return $query;
    }
   //---------------------funcion para  mostrar los datos almacenados en la tabla Clientedato  de forma ordenada desde el ultimo cliente
    public function getDataCliente()
    {
        $query = $this->con->query("SELECT * FROM `clientedato` order by idcliente DESC  limit 1");
        return $query;
    }

  /* ----------------------------------funcion para buscar e identificar y mostrar  el numero de documento del cliente */
    public function getClienteDatos($nitClient)
    {
        $query = $this->con->query("select * from cliente where ci = $nitClient ");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }
//-----Mostrar-----Operaciones-------------funcion para mostrar la lista de pedidos en la preventa muetra ademas el conteo de la cantidad de productos y la mumtiplicacion de la cantida por el precio y lo muestra como precio total
//---------------los ordena por numero de id producto 
    public function getPedidoTotalForFactura()
    {
        $query = $this->con->query("SELECT idpreventa,imagen,producto,precio, count( idproducto ) AS cantidad, precio*count( idproducto ) as totalPrecio, idproducto, pventa ,tipo FROM `preventa`  GROUP BY idproducto");
        return $query;
    }

 //------------------------muestra la cantidad de ficha que hay gereradas en factura solo por dia en el sistema y va a sumar 1 y los muestra como numficha donde la fecha es mayor igual la fecha inicial y menir a la ficnal
    public function getNumFicha($dateInicial, $dateFinal)
    {
        $query = $this->con->query("SELECT (COUNT(*) +1 ) as numficha FROM `ventatotal` WHERE fecha >= '$dateInicial 00:00:00' and fecha <= '$dateFinal 23:59:00'");
        return $query;
    }
//------------------------------insertando los datos de la venta  en la BD venta total
public function registrarVenta($nombre, $ci, $totalAPagar, $efectivo, $cambio, $idClientei, $codigoControl, $fechaVenta)
{
    $query = $this->con->query("INSERT INTO `ventatotal` (`idVentas`, `nombre`, `ci`, `fecha`, `totalApagar`, `efectivo`, `cambio`, `idClientei`, `codigoControl`) 
                                        VALUES (NULL, '$nombre', '$ci', '$fechaVenta', '$totalAPagar', '$efectivo', '$cambio', '$idClientei', '$codigoControl')");
    return $query;
}
//------------------------------- mostrando los datos almacenados de venta total
    public function getDatosVenta()
    {
        $query = $this->con->query("SELECT * FROM `ventatotal`");
        return $query;
    }
    /** XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXFUNCIONES PARA GENERAR UN NUEVO PEDIDO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX */
//--------------------------insertando los nuevos datos DE CANTIDADES Y PRECIOS  en datos venta-----------------
    public function registrarDatosVenta($cantidad, $descripcion, $precio, $total, $tipo, $fechaVenta, $codigoControl, $idVentas, $estado)
    {
        $query = $this->con->query("INSERT INTO `datosventa` (`idDatosVentas`, `cantidad`, `descripcion`, `precio`, `total`, `tipo`, `fechaVenta`, `codigoControl`, `idVentas`, `estado`)
                                      VALUES (NULL, '$cantidad', '$descripcion', '$precio', '$total', '$tipo', '$fechaVenta', '$codigoControl', '$idVentas', '$estado')");
        return $query;
    }
  /** --------------------INSERTANDO LOS DATOS DEL CLIENTE EN  DATOS VENTA TOTAL --------------------------------------$comentario  para asociar con la ficha ---------------------- */
    public function registrarDatosVentaTotal($cliente, $cantidad, $precio, $total, $codigoControl, $fechaVenta, $estado,$comentario)
    {
        $query = $this->con->query("INSERT INTO `datosventatotal` (`idVentas`, `cliente`, `cantidad`, `precio`, `total`, `codigoControl`, `fechaVenta`, `estado`, `comentario`) 
                                       VALUES (NULL, '$cliente', '$cantidad', '$precio', '$total', '$codigoControl', '$fechaVenta', '$estado','$comentario')");
        return $query;
    }
  /** --------------------INSERTANDO LOS DATOS DEL CLIENTE EN  DATOSCLIENTEVENTA------------------------------------------------------------ */
    public function registrarDatosClienteVenta($fechaVenta, $nitci, $cliente, $codigoControl, $idVentas, $estado)
    {
        $query = $this->con->query("INSERT INTO `datosclienteventa` (`idClienteVenta`, `fechaVenta`, `nitCliente`, `cliente`, `codigoControl`, `idVentas`, `estado`)
                                             VALUES (NULL, '$fechaVenta', '$nitci', '$cliente', '$codigoControl', '$idVentas', '$estado')");
        return $query;
    }
/** ------------------------INSERTA LOS DATOS CARACTERISTIVOS DE LA FACTURA-------------------------------------------------- */
    public function registrarDatosFacturaVenta($nit, $factura, $numeroAutorizacion, $codigoControl, $idVentas, $estado)
    {
        $query = $this->con->query("INSERT INTO `datosfacturaventa` (`idDatosFactura`, `nit`, `factura`, `numeroAutorizacion`, `codigoControl`, `idVentas`, `estado`)
                                              VALUES (NULL, '$nit', '$factura', '$numeroAutorizacion', '$codigoControl', '$idVentas', '$estado')");
        return $query;
    }
 /** XXXXXXXXXXXXXXXXXXXXXXXXXXXXXLIMPIANDO LA VISTA DE FACTURA  MIENTRAS HACE UN NUEVO PEDIDO XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX */
 /**   LIMPIA LOS DATOS DE LA BASE DE DATOS PREVENTA AL GENERAR UN NUEVO PEDIDO */
    public function cleanRegistroPreventa()
    {
        $query = $this->con->query("truncate `preventa`");
        return $query;
    }
        /**   LIMPIA LOS DATOS DEL CLIENTE  */
    public function cleanClientData()
    {
        $query = $this->con->query("truncate `clientedato`");
        return $query;
    }
    /**---------------------------CUENTA----------------- */
    /**-----------------muestra todos los gastos ordenados descendentemente-------------------- */
    public function getAllGastos()
    {

        $query = $this->con->query("SELECT * FROM `gastos` order by idgastos desc");
        return $query;
    }

  /** ---------------------Funcion para insertar los datos de los gastos ---------------------------------- */
    public function registerNewAccount($tipo, $descripcion, $entrada, $fechaRegistro, $usuario, $salida)
    {

        $query = $this->con->query("INSERT INTO `gastos` (`idgastos`, `descripcion`, `entrada`, `usuario`, `salida`, `tipo`,`fechaRegistro`) 
                                            VALUES (NULL, '$descripcion', '$entrada', '$usuario', '$salida', '$tipo','$fechaRegistro')");
        return $query;
    }

   /** ------------------------------Elimina la cuenta especifica segun id----------------------------------------------- */
    public function deleteAccount($idCuenta)
    {
        $query = $this->con->query("DELETE FROM `gastos` WHERE `idgastos` = $idCuenta");
        return $query;
    }

/** -------------------Inserta un Gasto especifico -------------------------------------------------- */
    public function updateAccount($tipo, $descripcion, $entrada, $fechaRegistro, $usuario, $salida, $idCuenta)
    {

        $query = $this->con->query("UPDATE `gastos` SET `descripcion` = '$descripcion', 
                                                                `entrada` = '$entrada',
                                                                `fechaRegistro` = '$fechaRegistro',
                                                                 `usuario` = '$usuario', 
                                                                 `salida` = '$salida', 
                                                                 `tipo` = '$tipo' WHERE `idgastos` = $idCuenta");
        return $query;
    }
  /** ---------Ver todos los pedidos para la tabla en la vista---------------------------------------------------- */
    public function getAllPedido()
    {
        $query = $this->con->query('SELECT * FROM pedido order by idpedido desc ');
        return $query;
    }
 /** ------------Registrar un nuevo pedido con el boton ----------------------------------------------- */
    public function registerNewPedido($descripcion, $total, $proveedor, $usuario, $fechaRegistro)
    {
        $query = $this->con->query("INSERT INTO `pedido` (`idPedido`, `descripcion`, `total`, `proveedor`, `usuario`, `fechaRegistro`) 
                                            VALUES (NULL, '$descripcion', '$total', '$proveedor', '$usuario', '$fechaRegistro')");
        return $query;
    }
 /**-------------eliminar un pedido ------------------------------------------------ */
    public function deletePedido($idPedido)
    {
        $query = $this->con->query("DELETE FROM pedido WHERE idPedido=$idPedido");
        return $query;
    }
 /**------------actuael nuevo---------------------------------------------------------- */
    public function updatePedido($descripcion, $total, $proveedor, $usuarioLogin, $fechaRegistro, $idPedido)
    {
        $query = $this->con->query("UPDATE `pedido` SET `descripcion` = '$descripcion',
                                                    `total` = '$total', `proveedor` = '$proveedor',
                                                     `usuario` = '$usuarioLogin', `fechaRegistro` = '$fechaRegistro'
                                                      WHERE `pedido`.`idPedido` = $idPedido ");
        return $query;
    }

  /**---------------------------------------------- */
    public function insertarComentarioFicha($idVenta, $comentario)
    {
        $query = $this->con->query("UPDATE `datosventatotal` SET `comentario` = '$comentario' WHERE `idVentas` = $idVenta");
        return $query;
    }

  /**----------Muestra las ventas  que tengan como estado no consolidado  y lo ordene por el id de la venta -------- */
    public function getAllVentas()
    {
        $query = $this->con->query('SELECT * FROM datosventatotal where estado=\'NoConsolidado\' order by idVentas ASC ');
        return $query;
    }
 /** ------------------- Cambia el estado a consolidado por medio del codigo de control en la tabla datos cliente venta------------------ */
    public function updateDatosclienteventa($codigoControl)
    {
        $query = $this->con->query("UPDATE `datosclienteventa` SET `estado` = 'Consolidado' WHERE `codigoControl` = '$codigoControl'");
        return $query;
    }
 /** ------------------- Cambia el estado a consolidado por medio del codigo de control en la tabla datos Factura venta------------------ */
    public function updateDatosfacturaventa($codigoControl)
    {
        $query = $this->con->query("UPDATE `datosfacturaventa` SET `estado` = 'Consolidado' WHERE `codigoControl` = '$codigoControl'");
        return $query;
    }
 /** ------------------- Cambia el estado a consolidado por medio del codigo de control en la tabla datos  venta------------------ */
    public function updateDatosventa($codigoControl)
    {
        $query = $this->con->query("UPDATE `datosventa` SET `estado` = 'Consolidado' WHERE `codigoControl` = '$codigoControl'");
        return $query;
    }
 /** ------------------- Cambia el estado a consolidado por medio del codigo de control en la tabla datos venta total------------------ */
    public function updateDatosventatotal($codigoControl)
    {
        $query = $this->con->query("UPDATE `datosventatotal` SET `estado` = 'Consolidado' WHERE `codigoControl` = '$codigoControl'");
        return $query;
    }
 /** -------------------Funcion que muestra la fecha actual de Ventas Consolidadas--------------------------------------------- */
    public function getVentasDia($fechaInicial,$fechaFinal)
    {
        $query = $this->con->query("SELECT * FROM `datosventatotal` WHERE fechaVenta >= '$fechaInicial' and fechaVenta < '$fechaFinal' and estado='Consolidado'");
        return $query;
    }

  /** -------Total de las ventas del dia---------Muestra la sumatoria de   las ventas consolidadas del dia ---------------------------------- */
    public function getVentasTotalesDia($fechaInicial,$fechaFinal)
    {
        $query = $this->con->query("SELECT SUM(total) as totalVentas FROM `datosventatotal` WHERE fechaVenta >= '$fechaInicial' and fechaVenta < '$fechaFinal' and estado='Consolidado'");
        return $query;
    }
    /**  muestra  toda la tabla datos venta donde concida con la fecha inicial y la fecha final y este consolidado */
    public function getVentasProductoByDia($fechaInicial,$fechaFinal)
    {
        $query = $this->con->query("SELECT * FROM `datosventa` WHERE fechaVenta >= '$fechaInicial' and fechaVenta < '$fechaFinal' and estado='Consolidado'");
        return $query;
    }
                /**   Uma el total de ventas po productos estipulados y consolidados en el rango de fecha */
    public function getVentasProductoTotalesDia($fechaInicial,$fechaFinal)
    {
        $query = $this->con->query("SELECT SUM(total) as totalVentas FROM `datosventa` WHERE fechaVenta >= '$fechaInicial' and fechaVenta < '$fechaFinal' and estado='Consolidado'");
        return $query;
    }
    /**  --Muestra el mes seleccionado-----toma el nombre del mes y lo muestra como parametro mes desde tabla datosventatotal y ordena por meses en orden ascendente------------ */
    public function getVentasMensuales()
    {

        $query = $this->con->query("SELECT MonthName(fecha) as mes FROM datosventatotal GROUP BY MONTH(fechaVenta) ASC");
        return $query;
    }
   /**--------muestra los dias del mes y la suma de sus ventas en forma ordenada en ta tabla de la vista de reporte--------------------------- */
    public function getSumaTotalVentasByMes($mes, $anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as totalVentas FROM datosventatotal WHERE MONTH(fechaVenta) = '$mes' AND YEAR(fechaVenta) = '$anio'");
        return $query;
    }
    /**-----------muestra la suma de ventas realizadas en todo el mes  es el ultimo mensaje que sale en el reporte ------------- */
    public function getTotalVentasByMes($mes, $anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, DAY(fechaVenta) as dia FROM datosventatotal WHERE MONTH(fechaVenta) = '$mes' AND YEAR(fechaVenta) = '$anio' GROUP BY DAY(fechaVenta) ASC");
        return $query;
    }
  
  /**-----------------muestra el mes y la suma de ventas del mismo en una lina ------------------------ */
    public function getTotalVentasByYear($anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as totalVentas FROM datosventatotal WHERE  YEAR(fechaVenta) = '$anio'");
        return $query;
    }
 /**---------------------muestra la suma de ventas de todos los meses del año---------------------------- */
    public function getTotalVentasByAnio($anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fechaVenta) as mes FROM datosventatotal  WHERE  YEAR(fechaVenta) = '$anio'   GROUP BY MONTH(fechaVenta) ASC");
        return $query;
    }

   /**------------dependiendo de la fecha actual muestre unicamente los meses y el total de sus ventas al frente-------------------------- */
    public function getTotalVentas6Meses()
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fechaVenta) as mes FROM datosventatotal  WHERE fechaVenta BETWEEN date_sub(now(), interval 6 month) AND NOW() GROUP BY MONTH(fechaVenta) ASC");
        return $query;
    }
/**-------------------muestra la suma  de las ventas de ultimos 6 meses------------------ */
    public function getGrandTotalVentas6Meses()
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as totalVentas FROM datosventatotal WHERE fechaVenta BETWEEN date_sub(now(), interval 6 month) AND NOW()");
        return $query;
    }
    /**---------sacando todo lo que se a gastado-------------- */
    public function getGatosDeLaEmpresa($fechaVentasI, $fechaVentasF)
    {
        $query = $this->con->query("SELECT  *
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }

    /**-----------sacando las entradas -------------------- */
    public function getEntradasDeLaEmpresa($fechaVentasI, $fechaVentasF)
    {
        $query = $this->con->query("SELECT  SUM(entrada) as totalEntrada
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }
 /**---------sacando el total de los gastos------------------- */

    public function getTotalGatosDeLaEmpresa($fechaVentasI, $fechaVentasF)
    {
        $query = $this->con->query("SELECT  SUM(salida) as totalSalida
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }
/**-----------------sacando las ganancias o utilidad entrada menos salida--------------------- */
    public function getUtilidadDeLaEmpresa($fechaVentasI, $fechaVentasF)
    {
        $query = $this->con->query("SELECT  (SUM(entrada) - SUM(salida)) as utilidad 
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }
    /** ===========REPORTES GRAFICOS====================== */
/**------VENTAS MENSUALES-----MUESTRA UN LISTADO DE LOS MESES  DE LA  TABLA VENTA TOTAL ----------------------------------------------------------- */
    public function getSumTotalVentasMensuales()
    {
        $query = $this->con->query("SELECT MonthName(fechaVenta) as mes FROM datosventatotal  GROUP BY MONTH(fechaVenta) ASC");
        return $query;
    }
/**-----VENTAS MENSUALES ------------MUESTRA LOS TOTALES POR MESES EN VENTAS ----------SOLO CUANDO ES CONSOLIDADO------------------------------------------- */
    public function getTotalVentasMensual()
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fechaVenta) as mes FROM datosventatotal where estado='Consolidado' GROUP BY MONTH(fechaVenta) ASC");
        return $query;
    }
/**----MUESTRA LA SUMA DE LOS TOTALES DE VENTAS DE TODO EL MES-------------------------------------------------- */
    public function getTotalVentas()
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fechaVenta) as mes FROM datosventatotal  where estado='Consolidado'   GROUP BY MONTH(fechaVenta) ASC");
        return $query;
    }
/**-------------GRAFICO DE VENTAS POR MES-------------------------------------------- */
    public function getTotalByMonthVentas($mes, $anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, DAY(fechaVenta) as dia FROM datosventatotal WHERE  estado='Consolidado' and MONTH(fechaVenta) = '$mes' AND YEAR(fechaVenta) = '$anio'   GROUP BY DAY(fechaVenta) ASC");
        return $query;
    }
/**--------------GRAFICO DE VENTAS POR 6 MESES--------------------------------------- */
    public function getTotal6MonthVentas()
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fechaVenta) as mes FROM datosventatotal WHERE estado='Consolidado'  and fechaVenta BETWEEN date_sub(now(), interval 6 month) AND NOW() GROUP BY MONTH(fechaVenta)  ASC");
        return $query;
    }
/**---------------GRAFICO DE VENTAS POR AÑO-------------------------------------------- */
    public function getTotalYearVentas($anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fechaVenta) as mes FROM datosventatotal  WHERE  estado='Consolidado' and  YEAR(fechaVenta) = '$anio'   GROUP BY MONTH(fechaVenta) ASC");
        return $query;
    }

/** -----------cambia en el menu el color de la pestaña seleccionada------si coincide el id del menu se cambia a otro color ---------------------------------------- */
    public function updateOpcionElegida($colorElegido,$idMenu)
    {

        $query = $this->con->query("UPDATE `menu` SET `color` = '$colorElegido' WHERE `idmenu` = $idMenu ");

        return $query;
    }
 /**------------------regresa el color por defecto------------------------------------ */
    public function updateOpcionDefecto($colorDefecto,$idMenu)
    {
        $query = $this->con->query("UPDATE `menu` SET `color` = '$colorDefecto' WHERE `idmenu` != $idMenu ");

        return $query;
    }
    /**----------PARA DASHBOARD PRINCIPAL ---------------------------------------------------- */
    public function getDataVentasTotal($fechaVentasI, $fechaVentasF)
    {
        $query = $this->con->query("SELECT descripcion as producto, cantidad, precio, SUM(cantidad * precio) as totalVendido,fechaVenta FROM `datosventa` 
                                           where estado='Consolidado' and fechaVenta  BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' group by descripcion");

        return $query;
    }
}
?>